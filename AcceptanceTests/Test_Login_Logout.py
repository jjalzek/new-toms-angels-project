import unittest
import django.test
from django.test import Client
from django.test import TestCase

from Sprint_1_App.models import Admin, Professor, TA
from Sprint_1_App.Views.AdminHome import AdminHome
from UserCommandHandler.ProgramOutput import ProgramOutput

class TestLoginLogout(TestCase):
    def setUp(self):
        self.client = Client()

    def test_login_admin(self):
        """
        User types in correct username and password
        User is directed to home page for their role
        """
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='scrum@1234',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'scrum@1234'})
        #self.assertEqual(resp.context['_userAccountRole'],'Admin')
        self.assertEqual(resp.url, '/adminHome')

    def test_incorrect_login_admin(self):
        """
        User types incorrect username and password
        User is told to re enter information into login page
        User enters correct information and is directed to admin home page
        """
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'pasasasasword'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')


    def test_incorrect_login_then_correct_login_admin(self):
        """
        User types incorrect username and password
        User is told to re enter information into login page
        User enters correct information and is directed to admin home page
        """
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'pasasasasword'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})
        self.assertEqual(resp.url, '/adminHome')

    def test_incorrect_epanther_admin(self):
        """
        User types incorrect username and password
        User is told to re enter information into login page
        User enters correct information and is directed to admin home page
        """
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'girlland', 'password': 'password'})
        self.assertEqual(resp.context['errormessage'],'Error, username girlland does not exist.')
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})
        self.assertEqual(resp.url, '/adminHome')

    def test_incorrect_epanther_and_password_admin(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'girlland', 'password': 'pasasasasword'})
        self.assertEqual(resp.context['errormessage'],'Error, username girlland does not exist.')


    def test_login_prof(self):
        #should go to professorHome
        self.rock = Professor.objects.create(epanther='rock',
                                            password='scrum@1234',
                                            firstName='rock',
                                            lastName='rocku',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/professorHome')

    def test_incorrect_login_prof(self):
        # should show error message
        self.rock = Professor.objects.create(epanther='rock',
                                             password='scrum@1234',
                                             firstName='rock',
                                             lastName='rocku',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum34'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')

    def test_incorrect_login_then_correct_login_prof(self):
        # should show error message
        self.rock = Professor.objects.create(epanther='rock',
                                             password='scrum@1234',
                                             firstName='rock',
                                             lastName='rocku',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum34'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/professorHome')

    def test_incorrect_epanther_prof(self):
        # should show error message
        self.rock = Professor.objects.create(epanther='rock',
                                             password='scrum@1234',
                                             firstName='rock',
                                             lastName='rocku',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'roll', 'password': 'scrum@1234'})
        self.assertEqual(resp.context['errormessage'],'Error, username roll does not exist.')

    def test_incorrect_epanther_and_password_prof(self):
        # should show error message
        self.rock = Professor.objects.create(epanther='rock',
                                             password='scrum@1234',
                                             firstName='rock',
                                             lastName='rocku',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'roll', 'password': '@1234'})
        self.assertEqual(resp.context['errormessage'],'Error, username roll does not exist.')

    def test_login_ta(self):
        # should go to taHome
        self.apoorv = TA.objects.create(epanther='prasada',
                                             password='scrum@1234',
                                             firstName='apoorv',
                                             lastName='prasad',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum@1234'})

    def test_incorrect_login_ta(self):
        # should show error message
        self.apoorv = TA.objects.create(epanther='apoorv',
                                             password='scrum@1234',
                                             firstName='apoorv',
                                             lastName='prasad',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum34'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')

    def test_incorrect_login__then_correct_login_ta(self):
        # should show error message
        self.apoorv = TA.objects.create(epanther='apoorv',
                                             password='scrum@1234',
                                             firstName='apoorv',
                                             lastName='prasad',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum34'})
        self.assertEqual(resp.context['errormessage'],'Error, incorrect password entered.')
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum@1234'})

    def test_incorrect_epanther_ta(self):
        # should show error message
        self.apoorv = TA.objects.create(epanther='apoorv',
                                             password='scrum@1234',
                                             firstName='apoorv',
                                             lastName='prasad',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'apo', 'password': 'scrum@1234'})
        self.assertEqual(resp.context['errormessage'],'Error, username apo does not exist.')

    def test_incorrect_epanther_and_password_ta(self):
        # should show error message
        self.apoorv = TA.objects.create(epanther='apoorv',
                                             password='scrum@1234',
                                             firstName='apoorv',
                                             lastName='prasad',
                                             phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'apo', 'password': 'scrum34'})
        self.assertEqual(resp.context['errormessage'],'Error, username apo does not exist.')

class TestNavigation(TestCase):

    def setUp(self):
        self.client = Client()
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='scrum@1234',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        self.apoorv = TA.objects.create(epanther='apoorv',
                                        password='scrum@1234',
                                        firstName='apoorv',
                                        lastName='prasad',
                                        phoneNumber='4140001111', )
        self.rock = Professor.objects.create(epanther='rock',
                                             password='scrum@1234',
                                             firstName='rock',
                                             lastName='rocku',
                                             phoneNumber='4140001111', )

    def test_navigate_to_adminHome_by_admin(self):
        # should redirect to adminHome
        resp = self.client.post('/', {'username': 'boyland', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/adminHome')

    def test_navigate_to_adminHome_by_prof(self):
        # should redirect to profHome
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/professorHome')
        render = self.client.get('/adminHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_adminHome_by_ta(self):
        # should redirect to taHome
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum@1234'})
        render = self.client.get('/adminHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_profHome_by_admin(self):
        resp = self.client.post('/', {'username': 'boyland', 'password': 'scrum@1234'})

        render = self.client.get('/profHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS A PROFESSOR")


    def test_navigate_to_profHome_by_prof(self):
        # should redirect to profHome
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/profHome')

    def test_nagivate_to_profHome_by_ta(self):
        # should redirect to taHome
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum@1234'})
        render = self.client.get('/profHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS A PROFESSOR")

    def test_navigate_to_taHome_by_admin(self):
        # should redirect to adminHome
        resp = self.client.post('/', {'username': 'boyland', 'password': 'scrum@1234'})
        render = self.client.get('/taHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS A TA")

    def test_navigate_to_taHome_by_prof(self):
        # should redirect to profHome
        resp = self.client.post('/', {'username': 'rock', 'password': 'scrum@1234'})
        render = self.client.get('/taHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS A TA")

    def test_navigate_to_taHome_by_ta(self):
        # should redirect to taHome
        resp = self.client.post('/', {'username': 'apoorv', 'password': 'scrum@1234'})
        self.assertEqual(resp.url, '/taHome')

    def test_navigate_to_adminHome_from_login_screen(self):
        # should redirect to login
        render = self.client.get('/adminHome')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_adminHome_from_logout_screen(self):
        # should redirect to login
        render = self.client.get('/logout_page')
        render = self.client.get('/adminHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_profHome_from_login_screen(self):
        # should redirect to login
        render = self.client.get('/profHome')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_profHome_from_logout_screen(self):
        # should redirect to logout screen
        render = self.client.get('/logout_page')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_taHome_from_login_screen(self):
        # should redirect to logout screen
        render = self.client.get('/taHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    def test_navigate_to_taHome_from_logout_screen(self):
        # should redirect to logout screen
        render = self.client.get('/logout_page')
        render = self.client.get('/taHome')
        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")





