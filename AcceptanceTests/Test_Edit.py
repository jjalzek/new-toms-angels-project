import unittest
import django.test
from django.test import Client
from django.test import TestCase

from Sprint_1_App.models import Admin
from Sprint_1_App.Views.AdminHome import AdminHome
from UserCommandHandler.ProgramOutput import ProgramOutput

class AcceptanceTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_navigate_to_edit_course(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')

    def test_edit_course_invalid_course_code(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse', {'selectElement': 'CS361', 'section': '401', 'days': 'MW', 'startTime': '1100', 'endTime': '1150'})
        self.assertEqual(resp.url, '/adminHome')
        self.assertEqual('Invalid course code! Course code already exists!')

    def test_edit_course_invalid_section_number(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse',
                                {'selectElement': 'CS361', 'newCourseCode': 'CS333', 'newCourseSectionNumber': 'hello',
                                'newMeetingDays': 'MWF', 'newStartTime': '0730', 'newEndTime': '0830'}) #Still red underlined? check EditCourse.py.....
        self.assertEqual('Invalid section number!')

    def test_edit_course_invalid_meeting_days(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse',
                                {'selectElement': 'CS361', 'newCourseCode': 'CS333', 'newCourseSectionNumber': '401',
                                'newMeetingDays': 'HELLO', 'newStartTime': '730', 'newEndTime': '830'}) #Still red underlined? check EditCourse.py.....
        self.assertEqual('Invalid meeting days!')

    def test_edit_course_invalid_start_time(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse',
                                {'selectElement': 'CS361', 'newCourseCode': 'CS333', 'newCourseSectionNumber': '401',
                                'newMeetingDays': 'MWF', 'newStartTime': '9999', 'newEndTime': '1150'}) #Still red underlined? check EditCourse.py.....
        self.assertEqual('Invalid starting time!')

    def test_edit_course_invalid_end_time(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse',
                                {'selectElement': 'CS361', 'newCourseCode': 'CS333', 'newCourseSectionNumber': '401',
                                'newMeetingDays': 'MWF', 'newStartTime': '1100', 'newEndTime': '9999'}) #Still red underlined? check EditCourse.py.....
        self.assertEqual('Invalid starting time!')

    def test_edit_course_valid(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editCourse'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editCourse')
        #now in admin edit course page
        resp = self.client.post('/editCourse',
                                {'selectElement': 'CS361', 'newCourseCode': 'CS333', 'newCourseSectionNumber': '401',
                                'newMeetingDays': 'MWF', 'newStartTime': '1100', 'newEndTime': '1150'}) #Still red underlined? check EditCourse.py.....
        #no return from editing, more editing tests can be seen in login/out and create
        self.assertEqual(resp.status_code, 200)

    def test_navigate_to_edit_lab(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editLab'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editLab')

    def test_edit_lab_valid(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editLab'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editLab')
        resp = self.client.post('/editLab',
                                {'selectElement': 'CS361', 'courseSection': '401', 'labSection': '603',
                            'days': 'MW', 'startTime': '1300', 'endTime': '1445' })  # input type = text"
        #no return from editing, more editing tests can be seen in login/out and create
        self.assertEqual(resp.status_code, 200)


    def test_navigate_to_edit_own_user_info(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editSelf'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editOwnUserInfo')


    def test_edit_own_user_info_valid(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editSelf'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editOwnUserInfo')
        resp = self.client.post('/editOwnUserInformation',
                                {'epanther': 'boyland', 'tempPassword': 'newpassword', 'firstName': 'JayJay',
                                'lastName': 'Boulder', 'phone': '4140009999', 'role': 'Prof'})  # input type = text"
        #no return from editing, more editing tests can be seen in login/out and create
        self.assertEqual(resp.status_code, 200)

    def test_navigate_to_edit_user(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'}) # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        #now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editUser'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editUser')

    def test_edit_user_valid(self):
        self.boyland = Admin.objects.create(epanther='boyland',
                                            password='password',
                                            firstName='John',
                                            lastName='Boyland',
                                            phoneNumber='4140001111', )
        resp = self.client.post('/', {'username': 'boyland', 'password': 'password'})  # input type = text"
        self.assertEqual(resp.url, '/adminHome')
        # now in admin home page
        resp = self.client.post('/adminHome', {'radioButtonGroup': 'editUser'})  # inputType = "radio"
        self.assertEqual(resp.url, '/editUser')



