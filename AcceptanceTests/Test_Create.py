from django.test import Client
from django.test import TestCase
from Sprint_1_App.models import Admin, Course, Lab, Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput


class NavigateToCreateWebPagesWhenLoggedInAsAdmin(TestCase):

    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()


    def loginAdminToAdminHomePage(self):
        redirect = self.client.post('/', {'username': 'boyland', 'password': 'password'})

        self.assertEqual(redirect.url, '/adminHome')
        self.assertContains(self.client.get('/adminHome'), "Logged in as boyland")


    def testNavigateToCreateProfessorWebPage(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)

        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createProf'})

        self.assertEqual(redirect.url, '/createProf')
        self.assertContains(self.client.get('/createProf'), "Logged in as boyland")


    def testNavigateToCreateTaWebPage(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)

        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createTA'})

        self.assertEqual(redirect.url, '/createTA')
        self.assertContains(self.client.get('/createTA'), "Logged in as boyland")


    def testNavigateToCreateCourseWebPage(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)

        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createCourse'})

        self.assertEqual(redirect.url, '/createCourse')
        self.assertContains(self.client.get('/createCourse'), "Logged in as boyland")


    def testNavigateToCreateLabWebPage(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)

        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createLab'})

        self.assertEqual(redirect.url, '/createLab')
        self.assertContains(self.client.get('/createLab'), "Logged in as boyland")


# End of class NavigateToCreateWebPagesWhenLoggedInAsAdmin.


class NavigateToCreateWebPagesWhenNotLoggedIn(TestCase):

    def setUp(self):
        self.client = Client()


    def testNavigateToCreateProfessorWebPage(self):
        render = self.client.get('/createProf')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateTaWebPage(self):
        render = self.client.get('/createTA')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateCourseWebPage(self):
        render = self.client.get('/createCourse')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateLabWebPage(self):
        render = self.client.get('/createLab')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


# End of class NavigateToCreateWebPagesWhenNotLoggedIn.

class NavigateToCreateWebPagesWhenLoggedInAsProf(TestCase):

    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()


    def testNavigateToCreateProfessorWebPageAsProf(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createProf')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateTaWebPageAsProf(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createTA')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateCourseWebPageAsProf(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createCourse')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateLabWebPageAsProf(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createLab')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


# End of class NavigateToCreateWebPagesWhenLoggedInAsProf.

class NavigateToCreateWebPagesWhenLoggedInAsTA(TestCase):

    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createTaAccountWithEpantherROCK()


    def testNavigateToCreateProfessorWebPageAsTA(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createProf')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateTaWebPageAsTA(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createTA')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateCourseWebPageAsTA(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createCourse')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


    def testNavigateToCreateLabWebPageAsTA(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createLab')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


# End of class NavigateToCreateWebPagesWhenLoggedInAsProf.


class AttemptToCreateCourse(TestCase):

    def testCreateNewCourseWithNewCodeAndSection(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateCourseWebpage()

        render = self.client.post('/createCourse', {'courseCode': 'CS361', 'section': '401', 'courseName': 'Intro Software', 'days': 'MWF',
                                                    'startTime': '11:00', 'endTime': '11:50'})

        self.assertEquals("Successfully created a new course: CS361 401 Intro Software MWF 11:00 11:50",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Successfully created a new course: CS361 401 Intro Software MWF 11:00 11:50")


    def testCreateNewCourseWithExistingCodeAndNewSection(self):
        self.createCourseCS361_401()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateCourseWebpage()

        render = self.client.post('/createCourse', {'courseCode': 'CS361', 'section': '999', 'courseName': 'Intro Software', 'days': 'MWF',
                                                    'startTime': '11:00', 'endTime': '11:50'})

        self.assertEquals("Successfully created a new course: CS361 999 Intro Software MWF 11:00 11:50",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Successfully created a new course: CS361 999 Intro Software MWF 11:00 11:50")


    def testCreateNewCourseWithAlreadyExistingCodeAndSection(self):
        self.createCourseCS361_401()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateCourseWebpage()

        render = self.client.post('/createCourse', {'courseCode': 'CS361', 'section': '401', 'courseName': 'Intro Software',
                                                    'days': 'MWF',
                                                    'startTime': '11:00', 'endTime': '11:50'})

        self.assertEquals("Course with this code and section already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Course with this code and section already exists!")


    ##### HELPER FUNCTIONS BELOW #####


    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()


    def navigateToCreateCourseWebpage(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createCourse'})
        self.assertEqual(redirect.url, '/createCourse')


    def createCourseCS361_401(self):
        Course.objects.create(courseCode = "CS361",
                              courseSection = "401",
                              meetingDays = "TR",
                              startingHHMM = "10:00",
                              endingHHMM = "10:50")


# End of class AttemptToCreateCourse.


class AttemptToCreateLab(TestCase):

    def testCreateNewLabSectionWithExistingCourseCodeAndExistingCourseSection(self):
        self.createCourseCS361_401()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateLabWebpage()

        render = self.client.post('/createLab', {'courseCode': 'CS361', 'courseSection' : '401', 'labSection': '001',
                                                 'days': 'MWF', 'startTime': '5:00', 'endTime': '5:50'})

        self.assertEquals("Successfully created a new lab: CS361 401 001 MWF 5:00 5:50",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Successfully created a new lab: CS361 401 001 MWF 5:00 5:50")


    def testCreateNewLabSectionWithExistingCourseCodeAndNonexistingCourseSection(self):
        self.createCourseCS361_401()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateLabWebpage()

        render = self.client.post('/createLab', {'courseCode': 'CS361', 'courseSection' : '999', 'labSection': '001',
                                                 'days': 'MWF', 'startTime': '5:00', 'endTime': '5:50'})

        self.assertEquals("Error, no course exists with the entered code and section!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course exists with the entered code and section!")


    def testCreateNewLabSectionWithNonexistingCourse(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateLabWebpage()

        render = self.client.post('/createLab', {'courseCode': 'CS999', 'courseSection' : '999', 'labSection': '001',
                                                 'days': 'MWF', 'startTime': '5:00', 'endTime': '5:50'})

        self.assertEquals("Error, no course exists with the entered code and section!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course exists with the entered code and section!")


    def testCreateTheSameLabSectionWithExistingCourseCodeAndExistingCourseSection(self):
        self.createCourseCS361_401()
        self.createLabCS361_401_001()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateLabWebpage()

        render = self.client.post('/createLab', {'courseCode': 'CS361', 'courseSection' : '401', 'labSection': '001',
                                                 'days': 'MWF', 'startTime': '5:00', 'endTime': '5:50'})

        self.assertEquals("Error, entered lab already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, entered lab already exists!")


    ##### HELPER FUNCTIONS BELOW #####


    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()


    def navigateToCreateLabWebpage(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createLab'})
        self.assertEqual(redirect.url, '/createLab')


    def createCourseCS361_401(self):
        Course.objects.create(courseCode = "CS361",
                              courseSection = "401",
                              meetingDays = "TR",
                              startingHHMM = "10:00",
                              endingHHMM = "10:50")


    def createLabCS361_401_001(self):
        Lab.objects.create(courseCode = "CS361",
                           courseSection = "401",
                           labSection='001',
                           meetingDays = "TR",
                           startingHHMM = "10:00",
                           endingHHMM = "10:50")


# End of class AttemptToCreateLab.


class AttemptToCreateProfessor(TestCase):

    def testCreateNewProfessorWithNonexistingEpanther(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateProfessorWebpage()

        render = self.client.post('/createProf', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                  'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Successfully created a new professor: ROCK PASSWORD JAYSON ROCK 414-999-9999",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Successfully created a new professor: ROCK PASSWORD JAYSON ROCK 414-999-9999")


    def testCreateNewProfessorWithAlreadyExistingAdminEpanther(self):
        HelpfulTestFunctions.createAdminAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateProfessorWebpage()

        render = self.client.post('/createProf', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                  'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    def testCreateNewProfessorWithAlreadyExistingProfessorEpanther(self):
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateProfessorWebpage()

        render = self.client.post('/createProf', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                  'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    def testCreateNewProfessorWithAlreadyExistingTaEpanther(self):
        HelpfulTestFunctions.createTaAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateProfessorWebpage()

        render = self.client.post('/createProf', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                  'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    ##### HELPER FUNCTIONS BELOW #####


    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()


    def navigateToCreateProfessorWebpage(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createProf'})
        self.assertEqual(redirect.url, '/createProf')


# End of class AttemptToCreateProfessor.


class AttemptToCreateTa(TestCase):

    def testCreateNewTaWithNonexistingEpanther(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateTaWebpage()

        render = self.client.post('/createTA', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Successfully created a new TA: ROCK PASSWORD JAYSON ROCK 414-999-9999",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Successfully created a new TA: ROCK PASSWORD JAYSON ROCK 414-999-9999")


    def testCreateNewTaWithAlreadyExistingAdminEpanther(self):
        HelpfulTestFunctions.createAdminAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateTaWebpage()

        render = self.client.post('/createTA', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    def testCreateNewTaWithAlreadyExistingProfessorEpanther(self):
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateTaWebpage()

        render = self.client.post('/createTA', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    def testCreateNewTaWithAlreadyExistingTaEpanther(self):
        HelpfulTestFunctions.createTaAccountWithEpantherROCK()
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToCreateTaWebpage()

        render = self.client.post('/createTA', {'ePanther': 'ROCK', 'tempPassword': 'PASSWORD', 'firstName': 'JAYSON',
                                                'lastName': 'ROCK', 'phone': "414-999-9999"})

        self.assertEquals("Account with this ePanther already exists!",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Account with this ePanther already exists!")


    ##### HELPER FUNCTIONS BELOW #####


    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()


    def navigateToCreateTaWebpage(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'createTA'})
        self.assertEqual(redirect.url, '/createTA')


# End of class AttemptToCreateTa.


class HelpfulTestFunctions:

    @staticmethod
    def createAdminBoylandAccount():
        Admin.objects.create(epanther = 'boyland',
                             password = 'password',
                             firstName = 'John',
                             lastName = 'Boyland',
                             phoneNumber = '414-000-1111')


    @staticmethod
    def loginAdminToAdminHomePage(classInstance):
        redirect = classInstance.client.post('/', {'username': 'boyland', 'password': 'password'})
        classInstance.assertEqual(redirect.url, '/adminHome')
        classInstance.assertContains(classInstance.client.get('/adminHome'), "Logged in as boyland")


    @staticmethod
    def createAdminAccountWithEpantherROCK():
        Admin.objects.create(epanther = "ROCK",
                             password = "PASSWORD",
                             firstName = "JAYSON",
                             lastName = "ROCK",
                             phoneNumber = "414-999-9999")


    @staticmethod
    def createProfessorAccountWithEpantherROCK():
        Professor.objects.create(epanther = "ROCK",
                                 password = "PASSWORD",
                                 firstName = "JAYSON",
                                 lastName = "ROCK",
                                 phoneNumber = "414-999-9999")


    @staticmethod
    def createTaAccountWithEpantherROCK():
        TA.objects.create(epanther = "ROCK",
                          password = "PASSWORD",
                          firstName = "JAYSON",
                          lastName = "ROCK",
                          phoneNumber = "414-999-9999")


# End of class HelpfulTestFunctions.
