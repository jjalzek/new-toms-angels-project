import unittest
import django.test
from django.test import Client
from django.test import TestCase

from Sprint_1_App.models import Admin, Professor, TA, Course, Lab
from Sprint_1_App.Views.AdminHome import AdminHome
from UserCommandHandler.ProgramOutput import ProgramOutput

class NavigateToAssignPagesAsAdmin(TestCase):
    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()

    ##### ACCEPTANCE TESTS FOR NAVIGATION FROM HOME PAGE #####
    # navigate tests for assign prof by ADMIN
    def test_navigate_to_assign_professor_to_course_as_admin(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'assignProf'})
        self.assertEqual(redirect.url, '/assignProf')

    # navigate tests for assign ta by ADMIN
    def test_navigate_to_assign_ta_to_course_as_admin(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'assignTA'})
        self.assertEqual(redirect.url, '/assignTA')

    # navigate tests for assign prof by ADMIN --> direct url
    def test_navigate_to_assign_ta_to_course_as_admin_direct(self):
        redirect = self.client.post('/', {'username': 'boyland', 'password': 'password'})
        redirect = self.client.get('/assignTA')
        #no url in response, but can check if the redirect failed
        self.assertEqual(redirect.status_code, 200)

    # navigate tests for assign prof by ADMIN --> direct url
    def test_navigate_to_assign_prof_to_course_as_admin_direct(self):
        redirect = self.client.post('/', {'username': 'boyland', 'password': 'password'})
        redirect = self.client.get('/assignProf')
        #no url in response, but can check if the redirect failed
        self.assertEqual(redirect.status_code, 200)

class NavigateToAssignPagesAsProfessor(TestCase):
    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()

    # navigate tests for assign prof by PROF
    # since a prof can't access /assignProf, they get redirected back to professorHome
    def test_navigate_to_assign_prof_to_course_as_prof(self):
        redirect = self.client.post('/', {'username': 'rock', 'password': 'password'})
        render = self.client.get('/createProf')

        self.assertContains(render, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    # navigate tests for assign ta by TA
    def test_navigate_to_assign_ta_to_course_as_prof(self):
        redirect = self.client.post('/professorHome', {'radioButtonGroup': 'assignTA'})
        self.assertEqual(redirect.url, '/assignTAToLab')

    # navigate tests for assign ta by TA --> direct post
    def test_navigate_to_assign_ta_to_course_as_prof_direct(self):
        redirect = self.client.post('/', {'username': 'boyland', 'password': 'password'})
        redirect = self.client.get('/assignProf')
        #no url in response, but can check if the redirect failed
        self.assertEqual(redirect.status_code, 200)


class NavigateToAssignPagesAsTA(TestCase):
    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createTaAccountWithEpantherAPOORV()
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()
    # navigate tests for assign ta by PROF with direct url
    def test_navigate_to_assign_ta_to_course_as_ta_direct(self):
        # should redirect to taHome
        redirect = self.client.post('/', {'username': 'apoorv', 'password': 'password'})
        redirect = self.client.get('/assignTA')
        self.assertContains(redirect, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")

    # navigate tests for assign prof by TA with direct url
    def test_navigate_to_assign_professor_to_course_as_ta_direct(self):
        # should redirect to taHome
        redirect = self.client.post('/', {'username': 'apoorv', 'password': 'password'})
        redirect = self.client.get('/assignProf')
        self.assertContains(redirect, "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!")


class AssignProfessorToCourseAsAdmin(TestCase):

    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()

    # test submitting professor with selected prof, course, lecture section
    def test_submit_assign_prof_form_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        HelpfulTestFunctions.createCourse1()
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("ROCK has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "ROCK has been successfully assigned to CS361 - 401.")

    # test submitting professor to course that professor is already assigned to
    def test_submit_assign_prof_to_already_assigned_course(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        HelpfulTestFunctions.createCourse1()
        self.navigateToAssignProfessorPageByAdmin()
        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'courseCode': 'CS361', 'section': '401'})

        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("ROCK has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "ROCK has been successfully assigned to CS361 - 401.")

    # test submitting professor with nothing selected
    def test_submit_assign_prof_nothing_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()
        render = self.client.post('/assignProf')

        self.assertEquals("Error, no professor with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no professor with the username was found.")

    # test submitting professor with epanther not selected
    def test_submit_assign_prof_epanther_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()
        render = self.client.post('/assignProf', {'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("Error, no professor with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no professor with the username was found.")

    # test submitting professor with course not selected
    def test_submit_assign_prof_course_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'section': '401'})

        self.assertEquals("Error, no course was found with course code NULL and section number 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number 401.")

    # test submitting professor with lecture not selected
    def test_submit_assign_prof_lecture_not_selected_by_admin_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'courseCode': 'CS361'})

        self.assertEquals("Error, no course was found with course code CS361 and section number NULL.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code CS361 and section number NULL.")

    # test submitting professor with epanther and course not selected
    def test_submit_assign_prof_epanther_and_course_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'section': '401'})

        self.assertEquals("Error, no professor with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no professor with the username was found.")

    # test submitting professor with epanther and lecture not selected
    def test_submit_assign_prof_epanther_and_lecture_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'courseCode': 'CS361'})

        self.assertEquals("Error, no professor with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no professor with the username was found.")

    # test submitting professor with course and lecture not selected
    def test_submit_assign_prof_course_and_lecture_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'epanther': 'ROCK'})

        self.assertEquals("Error, no course was found with course code NULL and section number NULL.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number NULL.")

    # test submitting professor with invalid course-lecture assignment
    def test_submit_assign_prof_invalid_lecture_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignProfessorPageByAdmin()

        render = self.client.post('/assignProf', {'epanther': 'ROCK', 'courseCode': 'CS361', 'section': '404'})

        self.assertEquals("Error, no course was found with course code CS361 and section number 404.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code CS361 and section number 404.")

####### Helper functions ########

    def navigateToAssignProfessorPageByAdmin(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'assignProf'})





class AssignTaToCourseByAdmin(TestCase):
    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createAdminBoylandAccount()
        HelpfulTestFunctions.createTaAccountWithEpantherAPOORV()
        HelpfulTestFunctions.createCourse1()
        HelpfulTestFunctions.createCourse2()
        HelpfulTestFunctions.createLab1()

    # test submitting ta with selected ta, course, lecture section
    def test_submit_assign_ta_form_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401', 'taRole':'lab-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta to lab that ta is already assigned to
    def test_submit_assign_ta_to_already_assigned_lab(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401', 'taRole': 'lab-ta'})

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401', 'taRole': 'lab-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta with nothing selected
    def test_submit_assign_ta_nothing_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA')

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with epanther not selected
    def test_submit_assign_ta_epanther_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'courseCode': 'CS361', 'section': '401', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with course not selected
    def test_submit_assign_ta_course_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'section': '401', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no course was found with course code NULL and section number 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number 401.")

    # test submitting ta with lecture not selected
    def test_submit_assign_ta_lecture_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no course was found with course code CS361 and section number NULL.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code CS361 and section number NULL.")

    # test submitting ta with epanther and course not selected
    def test_submit_assign_ta_epanther_and_course_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'section': '401', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with epanther and lecture not selected
    def test_submit_assign_ta_epanther_and_lecture_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'courseCode': 'CS361', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with course and lecture not selected
    def test_submit_assign_ta_course_and_lecture_not_selected_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no course was found with course code NULL and section number NULL.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number NULL.")

    # test submitting ta with invalid course-lecture assignment
    def test_submit_assign_ta_invalid_lecture_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()


        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '404', 'taRole': 'lab-ta'})

        self.assertEquals("Error, no course was found with course code CS361 and section number 404.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code CS361 and section number 404.")

    # test submitting ta as a lab TA
    def test_submit_assign_ta_as_lab_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401', 'taRole': 'lab-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta as a grader TA
    def test_submit_assign_ta_as_grader_by_admin(self):
        HelpfulTestFunctions.loginAdminToAdminHomePage(self)
        self.navigateToAssignTAPageByAdmin()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401', 'taRole': 'grader-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

####### Helper functions ########

    def navigateToAssignTAPageByAdmin(self):
        redirect = self.client.post('/adminHome', {'radioButtonGroup': 'assignTA'})





class AssignTaToCourseByProf(TestCase):
    def setUp(self):
        self.client = Client()
        HelpfulTestFunctions.createProfessorAccountWithEpantherROCK()
        HelpfulTestFunctions.createTaAccountWithEpantherAPOORV()
        HelpfulTestFunctions.createCourse1()
        HelpfulTestFunctions.createCourse2()
        HelpfulTestFunctions.createLab1()

    # test submitting ta with selected ta, course, lecture section
    def test_submit_assign_ta_form_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta to lab that ta is already assigned to
    def test_submit_assign_ta_to_already_assigned_lab(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401'})
        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta with nothing selected
    def test_submit_assign_ta_nothing_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA')

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with epanther not selected
    def test_submit_assign_ta_epanther_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'courseCode': 'CS361', 'section': '401'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with course not selected
    def test_submit_assign_ta_course_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'section': '401'})

        self.assertEquals("Error, no course was found with course code NULL and section number 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number 401.")

    # test submitting ta with lecture not selected
    def test_submit_assign_ta_lecture_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'courseCode': 'CS361'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with epanther and course not selected
    def test_submit_assign_ta_epanther_and_course_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'section': '401'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with epanther and lecture not selected
    def test_submit_assign_ta_epanther_and_lecture_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'courseCode': 'CS361'})

        self.assertEquals("Error, no TA with the username was found.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no TA with the username was found.")

    # test submitting ta with course and lecture not selected
    def test_submit_assign_ta_course_and_lecture_not_selected_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'epanther': 'APOORV'})

        self.assertEquals("Error, no course was found with course code NULL and section number NULL.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code NULL and section number NULL.")

    # test submitting ta with invalid course-lecture assignment
    def test_submit_assign_ta_invalid_lecture_by_prof(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA', {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '404'})

        self.assertEquals("Error, no course was found with course code CS361 and section number 404.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "Error, no course was found with course code CS361 and section number 404.")

    # test submitting ta as a lab TA
    def test_submit_assign_ta_as_lab_by_admin(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401',
                                   'taRole': 'lab-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

    # test submitting ta as a grader TA
    def test_submit_assign_ta_as_grader_by_admin(self):
        HelpfulTestFunctions.loginProfToProfHomePage(self)
        self.navigateToAssignTAPageByProfessor()

        render = self.client.post('/assignTA',
                                  {'epanther': 'APOORV', 'courseCode': 'CS361', 'section': '401',
                                   'taRole': 'grader-ta'})

        self.assertEquals("APOORV has been successfully assigned to CS361 - 401.",
                          ProgramOutput.returnProgramOutput())
        self.assertContains(render, "APOORV has been successfully assigned to CS361 - 401.")

####### Helper functions ########

    def navigateToAssignTAPageByProfessor(self):
        redirect = self.client.post('/professorHome', {'radioButtonGroup': 'assignTA'})



class HelpfulTestFunctions:

    @staticmethod
    def createAdminBoylandAccount():
        Admin.objects.create(epanther = 'boyland',
                             password = 'password',
                             firstName = 'John',
                             lastName = 'Boyland',
                             phoneNumber = '414-000-1111')

    @staticmethod
    def createProfessorAccountWithEpantherROCK():
        Professor.objects.create(epanther="ROCK",
                                 password="PASSWORD",
                                 firstName="JAYSON",
                                 lastName="ROCK",
                                 phoneNumber="414-999-9999")

    @staticmethod
    def createTaAccountWithEpantherAPOORV():
        TA.objects.create(epanther="APOORV",
                          password="PASSWORD",
                          firstName="APOORV",
                          lastName="PRASADA",
                          phoneNumber="414-999-9999")


    @staticmethod
    def loginAdminToAdminHomePage(classInstance):
        redirect = classInstance.client.post('/', {'username': 'boyland', 'password': 'password'})
        classInstance.assertEqual(redirect.url, '/adminHome')

    @staticmethod
    def loginProfToProfHomePage(classInstance):
        redirect = classInstance.client.post('/', {'username': 'ROCK', 'password': 'PASSWORD'})
        classInstance.assertEqual(redirect.url, '/professorHome')

    @staticmethod
    def loginTaToTaHomePage(classInstance):
        redirect = classInstance.client.post('/', {'username': 'APOORV', 'password': 'PASSWORD'})
        classInstance.assertEqual(redirect.url, '/taHome')


    @staticmethod
    def createCourse1():
        Course.objects.create(courseCode="CS361",
                              courseSection="401",
                              meetingDays="TR",
                              startingHHMM="10:00",
                              endingHHMM="10:50")

    @staticmethod
    def createCourse2():
        Course.objects.create(courseCode="CS337",
                              courseSection="404",
                              meetingDays="TR",
                              startingHHMM="11:00",
                              endingHHMM="11:50")

    @staticmethod
    def createLab1():
        Lab.objects.create(courseCode = "CS361",
                           courseSection = "401",
                           labSection='001',
                           meetingDays = "TR",
                           startingHHMM = "15:00",
                           endingHHMM = "15:50")

    @staticmethod
    def createLab2():
        Lab.objects.create(courseCode="CS337",
                           courseSection="404",
                           labSection='003',
                           meetingDays="TR",
                           startingHHMM="13:00",
                           endingHHMM="13:50")




# End of class HelpfulTestFunctions.


