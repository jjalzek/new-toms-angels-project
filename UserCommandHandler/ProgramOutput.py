class ProgramOutput:

    _programOutput = "default"


    @staticmethod
    def setProgramOutput(whatToOutput):
        ProgramOutput._programOutput = whatToOutput


    @staticmethod
    def returnProgramOutput():
        return ProgramOutput._programOutput


# End of class ProgramOutput.
