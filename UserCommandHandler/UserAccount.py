import Sprint_1_App
from Sprint_1_App.models import Admin, Professor, TA, Course, Lab
from django.shortcuts import render


# Static classes below containing useful functions that the classes above and other classes throughout the program call upon:


class OtherSessionFunctions:


    @staticmethod
    def displayTheLoginPageWithPromptToLoginAsAnAdminOrProfessor(request):
        return render(request, 'main/login.html',
                      {"errormessage": "ERROR, PLEASE LOG IN AS AN ADMIN OR PROFESSOR BEFORE ACCESSING THOSE COMMANDS!"})

    @staticmethod
    def displayTheLoginPageWithPromptToLoginAsAnAdmin(request):
        return render(request, 'main/login.html',
                      {"errormessage": "ERROR, PLEASE LOG IN AS AN ADMIN BEFORE ACCESSING THOSE COMMANDS!"})


    @staticmethod
    def displayTheLoginPageWithPromptToLoginAsAProfessor(request):
        return render(request, 'main/login.html',
                      {"errormessage": "ERROR, PLEASE LOG IN AS A PROFESSOR BEFORE ACCESSING THOSE COMMANDS!"})


    @staticmethod
    def displayTheLoginPageWithPromptToLoginAsATa(request):
        return render(request, 'main/login.html',
                      {"errormessage": "ERROR, PLEASE LOG IN AS A TA BEFORE ACCESSING THOSE COMMANDS!"})

    @staticmethod
    def getEpantherAssociatedWithThisSession(request):
        return "Logged in as " + request.session.get('epanther')


# End of class OtherSessionFunctions.


class DetermineAccountAssociatedWithASession:

    @staticmethod
    def returnEpantherAssociatedWithThisSession(request):
        return request.session.get('epanther')


    @staticmethod
    def isAnAdminAccountAssociatedWithThisSession(request):
        ePantherAssociatedWithThisSession = request.session.get('epanther')
        return DoesUserAccountExist.doesAdminAccountExist(ePantherAssociatedWithThisSession)


    @staticmethod
    def isAProfessorAccountAssociatedWithThisSession(request):
        ePantherAssociatedWithThisSession = request.session.get('epanther')
        return DoesUserAccountExist.doesProfessorAccountExist(ePantherAssociatedWithThisSession)


    @staticmethod
    def isATaAccountAssociatedWithThisSession(request):
        ePantherAssociatedWithThisSession = request.session.get('epanther')
        return DoesUserAccountExist.doesTaAccountExist(ePantherAssociatedWithThisSession)


# End of class DetermineAccountAssociatedWithASession.


class DoesUserAccountExist:

    @staticmethod
    def doesUserAccountExist(userEpanther):
        return DoesUserAccountExist.doesAdminAccountExist(userEpanther) or \
               DoesUserAccountExist.doesProfessorAccountExist(userEpanther) or \
               DoesUserAccountExist.doesTaAccountExist(userEpanther)


    @staticmethod
    def doesAdminAccountExist(adminEpanther):
        return Admin.objects.filter(epanther = adminEpanther).count() > 0


    @staticmethod
    def doesProfessorAccountExist(professorEpanther):
        return Professor.objects.filter(epanther = professorEpanther).count() > 0


    @staticmethod
    def doesTaAccountExist(taEpanther):
        return TA.objects.filter(epanther = taEpanther).count() > 0


# End of class DoesUserAccountExist.


class RetrieveUserAccount:

    @staticmethod
    def retrieveAccountWithGivenEpanther(epantherUsername):
        if DoesUserAccountExist.doesAdminAccountExist(epantherUsername):
            return RetrieveUserAccount.retrieveAdminWithGivenEpanther(epantherUsername)
        if DoesUserAccountExist.doesProfessorAccountExist(epantherUsername):
            return RetrieveUserAccount.retrieveProfessorWithGivenEpanther(epantherUsername)
        if DoesUserAccountExist.doesTaAccountExist(epantherUsername):
            return RetrieveUserAccount.retrieveTaWithGivenEpanther(epantherUsername)


    @staticmethod
    def retrieveAdminWithGivenEpanther(epantherUsername) -> Sprint_1_App.models.Admin:
        return Admin.objects.get(epanther = epantherUsername)


    @staticmethod
    def retrieveProfessorWithGivenEpanther(epantherUsername) -> Sprint_1_App.models.Professor:
        return Professor.objects.get(epanther = epantherUsername)


    @staticmethod
    def retrieveTaWithGivenEpanther(epantherUsername) -> Sprint_1_App.models.TA:
        return TA.objects.get(epanther = epantherUsername)


# End of class RetrieveUserAccount.


class OtherAccountFunctions:

    @staticmethod
    def determineUserAccountRole(epantherUsername):
        if DoesUserAccountExist.doesAdminAccountExist(epantherUsername):
            return "admin"
        if DoesUserAccountExist.doesProfessorAccountExist(epantherUsername):
            return "professor"
        if DoesUserAccountExist.doesTaAccountExist(epantherUsername):
            return "ta"
        return "NO ROLE FOUND"


    @staticmethod
    def returnListOfAllUserAccounts():
        allUserAccountObjects = Admin.objects.all() + Professor.objects.all() + TA.objects.all()
        return list(allUserAccountObjects.values_list())


    @staticmethod
    def returnListOfAllAdminAccounts():
        return list(Admin.objects.all().values_list())


    @staticmethod
    def returnListOfAllProfessorAccounts():
        return list(Professor.objects.all().values_list())


    @staticmethod
    def returnListOfAllTaAccounts():
        return list(TA.objects.all().values_list())


    @staticmethod
    def returnListOfAllCourseAccounts():
        return list(Course.objects.all().values_list())


    @staticmethod
    def returnListOfAllLabAccounts():
        return list(Lab.objects.all().values_list())


# End of class OtherAccountFunctions.
