from django.apps import AppConfig


class Sprint1AppConfig(AppConfig):
    name = 'Sprint_1_App'
