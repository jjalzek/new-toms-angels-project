from django.views import View
from django.shortcuts import render
from Sprint_1_App.models import Course
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class CreateCourse(View):

    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        return render(request, 'main/admin/create_course.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        CreateNewCourse.passInPostRequest(request)
        CreateNewCourse.attemptToCreateCourse()
        return render(request, 'main/admin/create_course.html', {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class CreateCourse.


class CreateNewCourse:

    _request = None
    _enteredCourseCode = ""
    _enteredCourseSectionNumber = ""
    _enteredCourseName = ""
    _enteredCourseMeetingDays = ""
    _enteredCourseStartingHHMM = ""
    _enteredCourseEndingHHMM = ""


    @staticmethod
    def passInPostRequest(request):
        CreateNewCourse._request = request


    @staticmethod
    def attemptToCreateCourse():
        CreateNewCourse._obtainEnteredCourseInfo()
        if CreateNewCourse.doesCourseAndSectionAlreadyExist(CreateNewCourse._enteredCourseCode,
                                                            CreateNewCourse._enteredCourseSectionNumber):
            return
        # Otherwise...
        CreateNewCourse._createNewCourseAccount()
        CreateNewCourse._outputNewCourseInformation()


    @staticmethod
    def doesCourseAndSectionAlreadyExist(courseCode, courseSection):
        if Course.objects.filter(courseCode = courseCode,
                                 courseSection = courseSection).count() > 0:
            ProgramOutput.setProgramOutput("Course with this code and section already exists!")
            return True
        return False


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredCourseInfo():
        CreateNewCourse._enteredCourseName = CreateNewCourse._request.POST["courseName"]
        CreateNewCourse._enteredCourseCode = CreateNewCourse._request.POST["courseCode"]
        CreateNewCourse._enteredCourseSectionNumber = CreateNewCourse._request.POST["section"]
        CreateNewCourse._enteredCourseMeetingDays = CreateNewCourse._request.POST["days"]
        CreateNewCourse._enteredCourseStartingHHMM = CreateNewCourse._request.POST["startTime"]
        CreateNewCourse._enteredCourseEndingHHMM = CreateNewCourse._request.POST["endTime"]


    @staticmethod
    def _createNewCourseAccount():
        Course.objects.create(courseCode = CreateNewCourse._enteredCourseCode,
                              courseSection = CreateNewCourse._enteredCourseSectionNumber,
                              courseName = CreateNewCourse._enteredCourseName,
                              meetingDays = CreateNewCourse._enteredCourseMeetingDays,
                              startingHHMM = CreateNewCourse._enteredCourseStartingHHMM,
                              endingHHMM = CreateNewCourse._enteredCourseEndingHHMM)

    @staticmethod
    def _outputNewCourseInformation():
        ProgramOutput.setProgramOutput("Successfully created a new course: " + \
                                       CreateNewCourse._enteredCourseCode + " " + \
                                       CreateNewCourse._enteredCourseSectionNumber + " " + \
                                       CreateNewCourse._enteredCourseName + " " + \
                                       CreateNewCourse._enteredCourseMeetingDays + " " + \
                                       CreateNewCourse._enteredCourseStartingHHMM + " " + \
                                       CreateNewCourse._enteredCourseEndingHHMM)


# End of class CreateNewCourse.

