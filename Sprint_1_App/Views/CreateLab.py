from django.views import View
from django.shortcuts import render
from Sprint_1_App.Views.CreateCourse import CreateNewCourse
from Sprint_1_App.models import Lab, Course
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class CreateLab(View):

    def get(self, request):
        courses = list(Course.objects.all().values_list())
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        return render(request, 'main/admin/create_lab.html',
                      {"courses": courses, "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        CreateNewLab.passInPostRequest(request)
        CreateNewLab.attemptToCreateLab()
        return render(request, 'main/admin/create_lab.html', {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class CreateLab.


class CreateNewLab:

    _request = None
    _enteredCourseCode = ""
    _enteredCourseSectionNumber = ""
    _enteredLabSectionNumber = ""
    _enteredLabMeetingDays = ""
    _enteredLabStartingHHMM = ""
    _enteredLabEndingHHMM = ""


    @staticmethod
    def passInPostRequest(request):
        CreateNewLab._request = request


    @staticmethod
    def attemptToCreateLab():
        CreateNewLab._obtainEnteredLabInfo()
        if not CreateNewLab._doesEnteredCourseAndSectionExist():
            return
        if CreateNewLab._doesLabAlreadyExist():
            return
        # Otherwise...
        CreateNewLab._CreateNewLabAccount()
        CreateNewLab._ConnectCourseToLab()
        CreateNewLab._outputNewLabInformation()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredLabInfo():
        CreateNewLab._enteredCourseCode = CreateNewLab._request.POST["courseCode"]
        CreateNewLab._enteredCourseSectionNumber = CreateNewLab._request.POST["courseSection"]
        CreateNewLab._enteredLabSectionNumber = CreateNewLab._request.POST["labSection"]
        CreateNewLab._enteredLabMeetingDays = CreateNewLab._request.POST["days"]
        CreateNewLab._enteredLabStartingHHMM = CreateNewLab._request.POST["startTime"]
        CreateNewLab._enteredLabEndingHHMM = CreateNewLab._request.POST["endTime"]


    @staticmethod
    def _doesEnteredCourseAndSectionExist():
        if not CreateNewCourse.doesCourseAndSectionAlreadyExist(CreateNewLab._enteredCourseCode,
                                                                CreateNewLab._enteredCourseSectionNumber):
            ProgramOutput.setProgramOutput("Error, no course exists with the entered code and section!")
            return False
        return True


    @staticmethod
    def _doesLabAlreadyExist():
        if Lab.objects.filter(courseCode = CreateNewLab._enteredCourseCode,
                              courseSection = CreateNewLab._enteredCourseSectionNumber,
                              labSection = CreateNewLab._enteredLabSectionNumber).count() > 0:
            ProgramOutput.setProgramOutput("Error, entered lab already exists!")
            return True
        else:
            return False


    @staticmethod
    def _CreateNewLabAccount():
        course = Course.objects.filter(courseCode=CreateNewLab._enteredCourseCode,
                                       courseSection=CreateNewLab._enteredCourseSectionNumber).first()
        course.save()
        Lab.objects.create(courseCode = CreateNewLab._enteredCourseCode,
                           courseSection = CreateNewLab._enteredCourseSectionNumber,
                           labSection = CreateNewLab._enteredLabSectionNumber,
                           meetingDays = CreateNewLab._enteredLabMeetingDays,
                           startingHHMM = CreateNewLab._enteredLabStartingHHMM,
                           endingHHMM = CreateNewLab._enteredLabEndingHHMM,
                           course = course)



    @staticmethod
    def _ConnectCourseToLab():
        course = Course.objects.filter(courseCode=CreateNewLab._enteredCourseCode,
                                       courseSection=CreateNewLab._enteredCourseSectionNumber).first()
        course.labSectionList += CreateNewLab._enteredLabSectionNumber + " "
        course.save()


    @staticmethod
    def _outputNewLabInformation():
        ProgramOutput.setProgramOutput("Successfully created a new lab: " + \
                                       CreateNewLab._enteredCourseCode + " " + \
                                       CreateNewLab._enteredCourseSectionNumber + " " + \
                                       CreateNewLab._enteredLabSectionNumber + " " + \
                                       CreateNewLab._enteredLabMeetingDays + " " + \
                                       CreateNewLab._enteredLabStartingHHMM + " " + \
                                       CreateNewLab._enteredLabEndingHHMM)


# End of class CreateNewLab.
