from django.shortcuts import render
from django.views import View
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import RetrieveUserAccount, OtherAccountFunctions


class EditUser(View):

    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        userlist = OtherAccountFunctions.returnListOfAllProfessorAccounts() +\
                   OtherAccountFunctions.returnListOfAllTaAccounts()
        return render(request, 'main/admin/edit_user.html',
                      {"userlist": userlist, "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        EditUserAccountInformation.passInPostRequest(request)
        EditUserAccountInformation.attemptToEditUserInformation()
        return render(request, 'main/admin/edit_user.html',
                     {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class EditUser.


class PopulateWebPageFieldsWithAccountInformation:

    _request = None
    _selectedUserAccount = None


    def presentUserAccountInformationOfSelectedEpanther(self, request):
        PopulateWebPageFieldsWithAccountInformation._request = request
        PopulateWebPageFieldsWithAccountInformation._fetchUserAccount()
        PopulateWebPageFieldsWithAccountInformation._redisplayWebpageWithPopulatedFields()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _fetchUserAccount():
        selectedEpanther = PopulateWebPageFieldsWithAccountInformation._request.POST["epanther"]
        PopulateWebPageFieldsWithAccountInformation._selectedUserAccount = RetrieveUserAccount.retrieveAccountWithGivenEpanther(selectedEpanther)


    @staticmethod
    def _redisplayWebpageWithPopulatedFields():
        return render(PopulateWebPageFieldsWithAccountInformation._request, 'main/admin/edit_user.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(PopulateWebPageFieldsWithAccountInformation._request),
                       "epanther": PopulateWebPageFieldsWithAccountInformation._selectedUserAccount.epanther,
                       "tempPassword" : PopulateWebPageFieldsWithAccountInformation._selectedUserAccount.epanther,
                       "firstName": PopulateWebPageFieldsWithAccountInformation._selectedUserAccount.epanther,
                       "lastName": PopulateWebPageFieldsWithAccountInformation._selectedUserAccount.epanther,
                       "phone": PopulateWebPageFieldsWithAccountInformation._selectedUserAccount.epanther})


# End of class PopulateWebPageFieldsWithAccountInformation.


class EditUserAccountInformation:

    _request = None
    _enteredEpanther = ""
    _enteredPassword = ""
    _enteredFirstName = ""
    _enteredLastName = ""
    _enteredPhone = ""


    @staticmethod
    def passInPostRequest(request):
        EditUserAccountInformation._request = request


    @staticmethod
    def attemptToEditUserInformation():
        EditUserAccountInformation._obtainEnteredUserInformation()
        # currently unsupported, but may add support to changing a user's epanther in the future:
        # if the epanther has been changed, check if there is already another account with that epanther in use
            # if there is, then display an error message, and return
        EditUserAccountInformation._updateUserAccountInformation()
        EditUserAccountInformation._outputUpdatedUserAccountInformation()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredUserInformation():
        EditUserAccountInformation._enteredEpanther = EditUserAccountInformation._request.POST["epanther"]
        EditUserAccountInformation._enteredPassword = EditUserAccountInformation._request.POST["tempPassword"]
        EditUserAccountInformation._enteredFirstName = EditUserAccountInformation._request.POST["firstName"]
        EditUserAccountInformation._enteredLastName = EditUserAccountInformation._request.POST["lastName"]
        EditUserAccountInformation._enteredPhone = EditUserAccountInformation._request.POST["phone"]


    @staticmethod
    def _updateUserAccountInformation():
        accountToBeEdited = RetrieveUserAccount.retrieveAccountWithGivenEpanther(EditUserAccountInformation._enteredEpanther)
        accountToBeEdited.epanther = EditUserAccountInformation._enteredEpanther
        accountToBeEdited.password = EditUserAccountInformation._enteredPassword
        accountToBeEdited.firstName = EditUserAccountInformation._enteredFirstName
        accountToBeEdited.lastName = EditUserAccountInformation._enteredLastName
        accountToBeEdited.phoneNumber = EditUserAccountInformation._enteredPhone
        accountToBeEdited.save()


    @staticmethod
    def _outputUpdatedUserAccountInformation():
        ProgramOutput.setProgramOutput("Successfully updated user information: " + \
                                       EditUserAccountInformation._enteredEpanther + " " +
                                       EditUserAccountInformation._enteredPassword + " " + \
                                       EditUserAccountInformation._enteredFirstName + " " +
                                       EditUserAccountInformation._enteredLastName + " " + \
                                       EditUserAccountInformation._enteredPhone)


# End of class EditUserAccountInformation.
