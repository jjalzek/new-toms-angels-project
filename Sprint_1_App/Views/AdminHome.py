from django.views import View
from django.shortcuts import render, redirect
from Sprint_1_App.models import Professor, TA, Course
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class AdminHome(View):

    _request = None
    _selectedRadioButton = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        profs = list(Professor.objects.all().values_list())
        tas = list(TA.objects.all().values_list())
        courses = list(Course.objects.all().values_list())
        return render(request, 'main/admin/home_admin.html', {'profs': profs, 'tas': tas, 'courses': courses,
                      "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        AdminHome._request = request
        AdminHome._determineWhatRadioButtonOptionWasSelected()
        return AdminHome._redirectAdminToAppropriateWebPage()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _determineWhatRadioButtonOptionWasSelected():
        AdminHome._selectedRadioButton = AdminHome._request.POST.get("radioButtonGroup")


    @staticmethod
    def _redirectAdminToAppropriateWebPage():
        if AdminHome._selectedRadioButton == "createProf":
            return redirect('create_professor_page')
        if AdminHome._selectedRadioButton == "createTA":
            return redirect('create_ta_page')
        if AdminHome._selectedRadioButton == "createCourse":
            return redirect('create_course_page')
        if AdminHome._selectedRadioButton == "createLab":
            return redirect('create_lab_page')
        if AdminHome._selectedRadioButton == "assignProf":
            return redirect('assign_professor_page')
        if AdminHome._selectedRadioButton == "assignTA":
            return redirect('assign_TA_page')
        if AdminHome._selectedRadioButton == "viewAccounts":
            return redirect('view_all_accounts_page')
        if AdminHome._selectedRadioButton == "editCourse":
            return redirect('edit_course_page')
        if AdminHome._selectedRadioButton == "editLab":
            return redirect('edit_lab_page')
        if AdminHome._selectedRadioButton == "editUser":
            return redirect('edit_user_page')
        if AdminHome._selectedRadioButton == "editSelf":
            return redirect('edit_own_user_info_page')


# End of class AdminHome.
