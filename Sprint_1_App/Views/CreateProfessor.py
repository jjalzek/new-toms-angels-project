from django.views import View
from django.shortcuts import render
from UserCommandHandler.UserAccount import DoesUserAccountExist
from Sprint_1_App.models import Professor
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class CreateProfessor(View):

    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        return render(request, 'main/admin/create_professor.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        CreateProfessorAccount.passInPostRequest(request)
        CreateProfessorAccount.attemptToCreateProfessor()
        return render(request, 'main/admin/create_professor.html', {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class CreateProfessor.


class CreateProfessorAccount:

    _request = None
    _enteredEpanther = ""
    _enteredPassword = ""
    _enteredFirstName = ""
    _enteredLastName = ""
    _enteredPhone = ""


    @staticmethod
    def passInPostRequest(request):
        CreateProfessorAccount._request = request


    @staticmethod
    def attemptToCreateProfessor():
        CreateProfessorAccount._obtainEnteredProfessorInfo()
        if DoesUserAccountExist.doesUserAccountExist(CreateProfessorAccount._enteredEpanther):
            ProgramOutput.setProgramOutput("Account with this ePanther already exists!")
            return
        # Otherwise...
        CreateProfessorAccount._createNewProfessorAccount()
        CreateProfessorAccount._outputNewProfessorInformation()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredProfessorInfo():
        CreateProfessorAccount._enteredEpanther = CreateProfessorAccount._request.POST["ePanther"]
        CreateProfessorAccount._enteredPassword = CreateProfessorAccount._request.POST["tempPassword"]
        CreateProfessorAccount._enteredFirstName = CreateProfessorAccount._request.POST["firstName"]
        CreateProfessorAccount._enteredLastName = CreateProfessorAccount._request.POST["lastName"]
        CreateProfessorAccount._enteredPhone = CreateProfessorAccount._request.POST["phone"]


    @staticmethod
    def _createNewProfessorAccount():
        Professor.objects.create(epanther = CreateProfessorAccount._enteredEpanther,
                                 password = CreateProfessorAccount._enteredPassword,
                                 firstName = CreateProfessorAccount._enteredFirstName,
                                 lastName = CreateProfessorAccount._enteredLastName,
                                 phoneNumber = CreateProfessorAccount._enteredPhone)


    @staticmethod
    def _outputNewProfessorInformation():
        ProgramOutput.setProgramOutput("Successfully created a new professor: " + \
                                       CreateProfessorAccount._enteredEpanther + " " +
                                       CreateProfessorAccount._enteredPassword + " " + \
                                       CreateProfessorAccount._enteredFirstName + " " +
                                       CreateProfessorAccount._enteredLastName + " " + \
                                       CreateProfessorAccount._enteredPhone)


# End of class CreateProfessorAccount.
