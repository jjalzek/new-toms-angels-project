from django.shortcuts import render
from django.views import View
from Sprint_1_App.models import Professor, Course
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions



class EditCourse(View):

    _request = None
    _newCourseCode = ""
    _newCourseSectionNumber = ""
    _newCourseMeetingDays = ""
    _newCourseStartingHHMM = ""
    _newCourseEndingHHMM = ""


    def get(self, request):
        if not (DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request) or
            DetermineAccountAssociatedWithASession.isAProfessorAccountAssociatedWithThisSession(request)):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdminOrProfessor(request)
        return render(request, 'main/admin/edit_course.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request),
                       "courses": list(Course.objects.all().values_list())})


    def post(self, request):
        return render(request, 'main/admin/edit_course.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request),
                       "courses": list(Course.objects.all().values_list())})


# End of class EditCourse
