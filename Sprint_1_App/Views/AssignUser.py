from django.shortcuts import render
from django.views import View
from Sprint_1_App.models import Course, Professor, TA, Lab
from UserCommandHandler.UserAccount import DoesUserAccountExist
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions, OtherAccountFunctions


class AssignProfessor(View):

    _professorList = None
    _courseList = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        AssignProfessor._professorList = OtherAccountFunctions.returnListOfAllProfessorAccounts()
        AssignProfessor._courseList = OtherAccountFunctions.returnListOfAllCourseAccounts()
        return render(request, 'main/admin/assign_prof.html',
                      {"profs": AssignProfessor._professorList,
                       "courses": AssignProfessor._courseList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        AssignUserToCourse.passInPostRequest(request)
        AssignUserToCourse.attemptToAssignProfessorToCourse()
        return render(request, 'main/admin/assign_prof.html',
                      {"outputMessage": ProgramOutput.returnProgramOutput(),
                       "profs": AssignProfessor._professorList,
                       "courses": AssignProfessor._courseList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    @staticmethod
    def _generateListOfProfessorsAndCourses():
        AssignProfessor._professorList = OtherAccountFunctions.returnListOfAllProfessorAccounts()
        AssignProfessor._courseList = OtherAccountFunctions.returnListOfAllCourseAccounts()

# End of class AssignProfessor


class AssignTA(View):

    _taList = None
    _courseList = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        AssignTA._taList = OtherAccountFunctions.returnListOfAllTaAccounts()
        AssignTA._courseList = OtherAccountFunctions.returnListOfAllCourseAccounts()
        return render(request, 'main/admin/assign_ta.html',
                      {"TAs": AssignTA._taList,
                       "courses": AssignTA._courseList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        AssignUserToCourse.passInPostRequest(request)
        AssignUserToCourse.attemptToAssignTaToCourse()
        return render(request, 'main/admin/assign_ta.html',
                      {"outputMessage": ProgramOutput.returnProgramOutput(),
                       "TAs": AssignTA._taList,
                       "courses": AssignTA._courseList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


# End of class AssignTA


class AssignTAToLab(View):

    _taList = None
    _courseList = None
    _labList = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAProfessorAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAProfessor(request)
        AssignTAToLab._taList = OtherAccountFunctions.returnListOfAllTaAccounts()
        AssignTAToLab._courseList = list(Course.objects.filter
                                         (professorEpanther=request.session.get('epanther')).values_list())
        AssignTAToLab._labList = list(Lab.objects.filter
                                      (course__professorEpanther=request.session.get('epanther')).values_list())
        return render(request, 'main/professor/assign_ta_to_lab.html',
                      {"TAs": AssignTAToLab._taList,
                       "courses": AssignTAToLab._courseList,
                       "labs": AssignTAToLab._labList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        AssignUserToCourse.passInPostRequest(request)
        AssignUserToCourse.attemptToAssignTaToLab()
        return render(request, 'main/professor/assign_ta_to_lab.html',
                      {"outputMessage": ProgramOutput.returnProgramOutput(),
                       "TAs": AssignTAToLab._taList,
                       "courses": AssignTAToLab._courseList,
                       "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


# End of class AssignTAToLab


class AssignUserToCourse:

    _request = None
    enteredEpantherId = ""
    enteredCourseCode = ""
    enteredCourseSection = ""
    enteredLabSection = ""
    courseToAssignTo = None


    @staticmethod
    def passInPostRequest(request):
        AssignUserToCourse._request = request


    @staticmethod
    def attemptToAssignProfessorToCourse():
        AssignUserToCourse._obtainEnteredUserEpanther()
        if not _AssignProfessorToCourse._isEnteredEpantherAProfessor():
            return
        AssignUserToCourse._obtainCourseToAssignTo()
        if not AssignUserToCourse._doesCourseAndSectionExist():
            return
        _AssignProfessorToCourse._addProfessorEpantherToCourse()
        AssignUserToCourse._outputUpdatedCourseInformation()


    @staticmethod
    def attemptToAssignTaToCourse():
        AssignUserToCourse._obtainEnteredUserEpanther()
        if not _AssignTaToCourse._isEnteredEpantherATa():
            return
        AssignUserToCourse._obtainCourseToAssignTo()
        if not AssignUserToCourse._doesCourseAndSectionExist():
            return
        if _AssignTaToCourse.doesCourseAlreadyContainEnteredTa():
            return
        _AssignTaToCourse._addTaEpantherToCourse()
        AssignUserToCourse._outputUpdatedCourseInformation()

    @staticmethod
    def attemptToAssignTaToLab():
        AssignUserToCourse._obtainEnteredUserEpanther()
        if not _AssignTaToCourse._isEnteredEpantherATa():
            return
        AssignUserToCourse._obtainLabToAssignTo()
        AssignUserToCourse.labToAssignTo = AssignUserToCourse._retrieveLabAccount()
        AssignUserToCourse.labToAssignTo.taEpanther = AssignUserToCourse.enteredEpantherId
        ProgramOutput.setProgramOutput(AssignUserToCourse.enteredEpantherId + " has been successfully assigned to " +
                                       AssignUserToCourse.enteredCourseCode + " - " +
                                       AssignUserToCourse.enteredCourseSection + " in lab " +
                                       AssignUserToCourse.enteredLabSection + ".")


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredUserEpanther():
        AssignUserToCourse.enteredEpantherId = AssignUserToCourse._request.POST.get("epanther", "")


    @staticmethod
    def _obtainCourseToAssignTo():
        AssignUserToCourse.enteredCourseCode = AssignUserToCourse._request.POST.get("courseCode", "NULL")
        AssignUserToCourse.enteredCourseSection = AssignUserToCourse._request.POST.get("section", "NULL")


    @staticmethod
    def _obtainLabToAssignTo():
        AssignUserToCourse.enteredLabSection = AssignUserToCourse._request.POST.get("labCode", "NULL")


    @staticmethod
    def _doesCourseAndSectionExist():
        if Course.objects.filter(courseCode = AssignUserToCourse.enteredCourseCode,
                                 courseSection = AssignUserToCourse.enteredCourseSection).count() == 0:
            ProgramOutput.setProgramOutput("Error, no course was found with course code " +
                                           AssignUserToCourse.enteredCourseCode + " and section number " +
                                           AssignUserToCourse.enteredCourseSection + ".")
            return False
        return True


    @staticmethod
    def _retrieveCourseAccount():
        return Course.objects.filter(courseCode = AssignUserToCourse.enteredCourseCode,
                                     courseSection = AssignUserToCourse.enteredCourseSection).first()

    @staticmethod
    def _retrieveLabAccount():
        return Lab.objects.filter(labSection = AssignUserToCourse.enteredLabSection)



    @staticmethod
    def _outputUpdatedCourseInformation():
        ProgramOutput.setProgramOutput(AssignUserToCourse.enteredEpantherId + " has been successfully assigned to " +
                                       AssignUserToCourse.enteredCourseCode + " - " +
                                       AssignUserToCourse.enteredCourseSection + ".")


# End of class AssignToCourse.


# Below are private classes, which are full of private helper methods that are used in the above AssignToCourse class.


class _AssignProfessorToCourse:

    @staticmethod
    def _isEnteredEpantherAProfessor():
        if not DoesUserAccountExist.doesProfessorAccountExist(AssignUserToCourse.enteredEpantherId):
            ProgramOutput.setProgramOutput("Error, no professor with the username " +
                                           AssignUserToCourse.enteredEpantherId + "was found.")
            return False
        return True


    @staticmethod
    def _addProfessorEpantherToCourse():
        AssignUserToCourse.courseToAssignTo = AssignUserToCourse._retrieveCourseAccount()
        AssignUserToCourse.courseToAssignTo.professorEpanther = AssignUserToCourse.enteredEpantherId
        AssignUserToCourse.courseToAssignTo.save()

# End of class _AssignProfessorToCourse.


class _AssignTaToCourse:

    @staticmethod
    def _isEnteredEpantherATa():
        if not DoesUserAccountExist.doesTaAccountExist(AssignUserToCourse.enteredEpantherId):
            ProgramOutput.setProgramOutput("Error, no TA with the username " +
                                           AssignUserToCourse.enteredEpantherId + "was found.")
            return False
        return True


    @staticmethod
    def doesCourseAlreadyContainEnteredTa():
        AssignUserToCourse.courseToAssignTo = AssignUserToCourse._retrieveCourseAccount()
        if AssignUserToCourse.enteredEpantherId in AssignUserToCourse.courseToAssignTo.taEpantherList:
            ProgramOutput.setProgramOutput("Error, that TA is already assigned to this course!")
            return True
        return False


    @staticmethod
    def _addTaEpantherToCourse():
        AssignUserToCourse.courseToAssignTo = AssignUserToCourse._retrieveCourseAccount()
        AssignUserToCourse.courseToAssignTo.taEpantherList += AssignUserToCourse.enteredEpantherId + " "
        AssignUserToCourse.courseToAssignTo.save()


# End of class _AssignTaToCourse.

class _AssignTAToLab:

    @staticmethod
    def _addTaEpantherToLab():
        pass
