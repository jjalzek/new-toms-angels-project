from django.shortcuts import render
from django.views import View

from UserCommandHandler import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class EditOwnUserInfo(View):

    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        return render(request, 'main/admin/edit_own_user_info.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        return render(request, 'main/admin/edit_own_user_info.html.html',
                      {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class EditOwnUserInfo
