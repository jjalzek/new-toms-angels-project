from django.views import View
from django.shortcuts import render, redirect
from Sprint_1_App.models import Professor, TA, Course
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions, OtherAccountFunctions


class TaHome(View):

    _request = None
    _selectedRadioButton = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isATaAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsATa(request)
        profs = OtherAccountFunctions.returnListOfAllProfessorAccounts()
        tas = OtherAccountFunctions.returnListOfAllTaAccounts()
        courses = OtherAccountFunctions.returnListOfAllCourseAccounts()
        return render(request, 'main/ta/home_ta.html', {'profs': profs, 'tas': tas, 'courses': courses,
                      "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        TaHome._request = request
        TaHome._determineWhatRadioButtonOptionWasSelected()
        return TaHome._redirectAdminToAppropriateWebPage()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _determineWhatRadioButtonOptionWasSelected():
        TaHome._selectedRadioButton = TaHome._request.POST.get("radioButtonGroup")


    @staticmethod
    def _redirectAdminToAppropriateWebPage():
        if TaHome._selectedRadioButton == "viewAccounts":
            return redirect('view_all_accounts_page')
        if TaHome._selectedRadioButton == "editSelf":
            return redirect('edit_own_user_info_page')


# End of class TaHome.