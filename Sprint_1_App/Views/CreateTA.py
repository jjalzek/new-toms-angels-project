from django.views import View
from django.shortcuts import render
from UserCommandHandler.UserAccount import DoesUserAccountExist
from Sprint_1_App.models import TA
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class CreateTA(View):

    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdmin(request)
        return render(request, 'main/admin/create_TA.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        CreateTaAccount.passInPostRequest(request)
        CreateTaAccount.attemptToCreateTa()
        return render(request, 'main/admin/create_TA.html', {"outputMessage": ProgramOutput.returnProgramOutput()})


# End of class CreateTA.


class CreateTaAccount:

    _request = None
    _enteredEpanther = ""
    _enteredPassword = ""
    _enteredFirstName = ""
    _enteredLastName = ""
    _enteredPhone = ""


    @staticmethod
    def passInPostRequest(request):
        CreateTaAccount._request = request


    @staticmethod
    def attemptToCreateTa():
        CreateTaAccount._obtainEnteredTaInfo()
        if DoesUserAccountExist.doesUserAccountExist(CreateTaAccount._enteredEpanther):
            ProgramOutput.setProgramOutput("Account with this ePanther already exists!")
            return
        # Otherwise...
        CreateTaAccount._createNewTaAccount()
        CreateTaAccount._outputNewTaInformation()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredTaInfo():
        CreateTaAccount._enteredEpanther = CreateTaAccount._request.POST["ePanther"]
        CreateTaAccount._enteredPassword = CreateTaAccount._request.POST["tempPassword"]
        CreateTaAccount._enteredFirstName = CreateTaAccount._request.POST["firstName"]
        CreateTaAccount._enteredLastName = CreateTaAccount._request.POST["lastName"]
        CreateTaAccount._enteredPhone = CreateTaAccount._request.POST["phone"]


    @staticmethod
    def _createNewTaAccount():
        TA.objects.create(epanther = CreateTaAccount._enteredEpanther,
                          password = CreateTaAccount._enteredPassword,
                          firstName = CreateTaAccount._enteredFirstName,
                          lastName = CreateTaAccount._enteredLastName,
                          phoneNumber = CreateTaAccount._enteredPhone)


    @staticmethod
    def _outputNewTaInformation():
        ProgramOutput.setProgramOutput("Successfully created a new TA: " + \
                                       CreateTaAccount._enteredEpanther + " " +
                                       CreateTaAccount._enteredPassword + " " + \
                                       CreateTaAccount._enteredFirstName + " " +
                                       CreateTaAccount._enteredLastName + " " + \
                                       CreateTaAccount._enteredPhone)


# End of class CreateTaAccount.

