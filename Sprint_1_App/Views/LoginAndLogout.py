from django.views import View
from django.shortcuts import render, redirect
from UserCommandHandler.ProgramOutput import ProgramOutput
from UserCommandHandler.UserAccount import DoesUserAccountExist, RetrieveUserAccount


class LoginUserToHomePage(View):

    _userAccountRole = ""


    def get(self, request):
        return render(request, 'main/login.html')


    def post(self, request):
        LoginUser.passInPostRequest(request)
        if not LoginUser.areEnteredUsernameAndPasswordValid():
            return render(request, 'main/login.html', {"errormessage": ProgramOutput.returnProgramOutput()})
        # Otherwise...
        LoginUser.saveUserEpantherAssociatedWithThisSession()
        LoginUserToHomePage._userAccountRole = LoginUser.determineUserAccountRole()
        return LoginUserToHomePage._redirectUserToAppropriateHomePage()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _redirectUserToAppropriateHomePage():
        if LoginUserToHomePage._userAccountRole == "admin":
            return redirect('admin_home_page')
        if LoginUserToHomePage._userAccountRole == "professor":
            return redirect('professor_home_page')
        if LoginUserToHomePage._userAccountRole == "ta":
            return redirect('ta_home_page')


# End of class LoginUserToHomePage.


class LoginUser:

    _request = None
    _enteredEpanther = ""
    _enteredPassword = ""
    _userAccountRole = ""


    @staticmethod
    def passInPostRequest(request):
        LoginUser._request = request


    @staticmethod
    def saveUserEpantherAssociatedWithThisSession():
        LoginUser._request.session['epanther'] = LoginUser._enteredEpanther


    @staticmethod
    def areEnteredUsernameAndPasswordValid():
        LoginUser._obtainEnteredUsernameAndPassword()
        if LoginUser._doesEpantherNotExist():
            return False
        if LoginUser._doesEpantherNotMatchPassword():
            return False
        return True


    @staticmethod
    def determineUserAccountRole():
        if DoesUserAccountExist.doesAdminAccountExist(LoginUser._enteredEpanther):
            return "admin"
        if DoesUserAccountExist.doesProfessorAccountExist(LoginUser._enteredEpanther):
            return "professor"
        if DoesUserAccountExist.doesTaAccountExist(LoginUser._enteredEpanther):
            return "ta"
        return "NO ROLE FOUND"


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _obtainEnteredUsernameAndPassword():
        LoginUser._enteredEpanther = LoginUser._request.POST["username"]
        LoginUser._enteredPassword = LoginUser._request.POST["password"]


    @staticmethod
    def _doesEpantherNotExist():
        if not DoesUserAccountExist.doesUserAccountExist(LoginUser._enteredEpanther):
            ProgramOutput.setProgramOutput("Error, username " + LoginUser._enteredEpanther + " does not exist.")
            return True
        return False


    @staticmethod
    def _doesEpantherNotMatchPassword():
        LoginUser._userAccount = RetrieveUserAccount.retrieveAccountWithGivenEpanther(LoginUser._enteredEpanther)
        if LoginUser._userAccount.password != LoginUser._enteredPassword:
            ProgramOutput.setProgramOutput("Error, incorrect password entered.")
            return True
        return False


# End of class LoginUser.


class LogoutUser(View):

    _request = None


    def get(self, request):
        LogoutUser._request = request
        LogoutUser.removeEpantherAssociatedWithThisSession()
        return render(request, 'main/logout.html')


    @staticmethod
    def removeEpantherAssociatedWithThisSession():
        del LogoutUser._request.session['epanther']


# End of class Logout.
