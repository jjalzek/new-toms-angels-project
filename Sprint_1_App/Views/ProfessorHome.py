from django.views import View
from django.shortcuts import render, redirect
from Sprint_1_App.models import Professor, TA, Course
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions, OtherAccountFunctions


class ProfessorHome(View):

    _request = None
    _selectedRadioButton = None


    def get(self, request):
        if not DetermineAccountAssociatedWithASession.isAProfessorAccountAssociatedWithThisSession(request):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAProfessor(request)
        profs = OtherAccountFunctions.returnListOfAllProfessorAccounts()
        tas = OtherAccountFunctions.returnListOfAllTaAccounts()
        courses = OtherAccountFunctions.returnListOfAllCourseAccounts()
        return render(request, 'main/professor/home_professor.html', {'profs': profs, 'tas': tas, 'courses': courses,
                      "userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request)})


    def post(self, request):
        ProfessorHome._request = request
        ProfessorHome._determineWhatRadioButtonOptionWasSelected()
        return ProfessorHome._redirectAdminToAppropriateWebPage()


    ##### PRIVATE HELPER FUNCTIONS BELOW #####


    @staticmethod
    def _determineWhatRadioButtonOptionWasSelected():
        ProfessorHome._selectedRadioButton = ProfessorHome._request.POST.get("radioButtonGroup")


    @staticmethod
    def _redirectAdminToAppropriateWebPage():
        if ProfessorHome._selectedRadioButton == "assignTA":
            return redirect('assign_TA_to_lab_page')
        if ProfessorHome._selectedRadioButton == "editSelf":
            return redirect('edit_own_user_info_page')


# End of class ProfHome.