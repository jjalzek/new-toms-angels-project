from django.shortcuts import render
from django.views import View

from Sprint_1_App.models import Lab
from UserCommandHandler.UserAccount import DetermineAccountAssociatedWithASession, OtherSessionFunctions


class EditLab(View):

    def get(self, request):
        if not (DetermineAccountAssociatedWithASession.isAnAdminAccountAssociatedWithThisSession(request) or
            DetermineAccountAssociatedWithASession.isAProfessorAccountAssociatedWithThisSession(request)):
            return OtherSessionFunctions.displayTheLoginPageWithPromptToLoginAsAnAdminOrProfessor(request)
        return render(request, 'main/admin/edit_lab.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request),
                       "labs": list(Lab.objects.values_list())})


    def post(self, request):
        return render(request, 'main/admin/edit_lab.html',
                      {"userEpanther": OtherSessionFunctions.getEpantherAssociatedWithThisSession(request),
                       "labs": list(Lab.objects.values_list())})


# End of class EditLab
