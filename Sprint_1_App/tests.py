from django.test import TestCase, Client

from .models import Course
from .models import Professor
from .models import Admin
from .models import TA
from .models import Lab

# Create your tests here.
#create and test course, prof, and TA
class DatabaseTests(TestCase):
    def test_default_course(self):
        c = Course()
        self.assertEqual(c.courseName, "Default Course")
        self.assertEqual(c.courseCode, "CS000")
        self.assertEqual(c.courseSection, "000")
        self.assertEqual(c.courseDescription, "")
        self.assertEqual(c.courseDescription, "")
        self.assertEqual(c.meetingDays, "")
        self.assertEqual(c.startingHHMM, "0000")
        self.assertEqual(c.endingHHMM, "0000")
        self.assertEqual(c.hasLab, False)
        self.assertEqual(c.professorEpanther, "")
        self.assertEqual(c.taEpantherList, "")

    def test_new_prof(self):
        Professor.objects.create(epanther="rock", password="password",
                                 firstName="jayson", lastName="rock",
                                 phoneNumber="9999999999")

        self.assertEqual(Professor.objects.filter(epanther="rock").__str__(), "<QuerySet [<Professor: Professor object (1)>]>")

    def test_new_TA(self):
        TA.objects.create(epanther="apoorv", password="password",
                                 firstName="apoorv", lastName="prasada",
                                 phoneNumber="9999999999")
        self.assertEqual(TA.objects.filter(epanther="apoorv").__str__(),
                         "<QuerySet [<TA: TA object (1)>]>")