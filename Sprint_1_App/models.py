from django.db import models


# Create your models here.
# class YourClass:
#   def command(self,inStr):
#     return inStr

class Admin(models.Model):

    epanther = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    firstName = models.CharField(max_length=50, default="")
    lastName = models.CharField(max_length=50, default="")
    phoneNumber = models.CharField(max_length=50, default="")

# End of class Admin.


class Course(models.Model):

    courseCode = models.CharField(max_length=50, default="CS000")
    courseSection = models.CharField(max_length=50, default="000")
    courseName = models.CharField(max_length=50, default="")
    courseDescription = models.CharField(max_length = 500, default="")
    meetingDays = models.CharField(max_length = 10, default="")
    startingHHMM = models.CharField(max_length = 10, default="0000")
    endingHHMM = models.CharField(max_length = 10, default="0000")
    hasLab = models.BooleanField(default=False)  # Will remain false until a new lab is created and assigned to this course.
    professorEpanther = models.CharField(max_length=15, default="")
    taEpantherList = models.CharField(max_length = 50, default="")
    labSectionList = models.CharField(max_length = 4, default="")

 # End of class Course.


class Lab(models.Model):
    DEFAULT_PK = 1

    courseCode = models.CharField(max_length=50, default="CS000")
    courseSection = models.CharField(max_length=50, default="000")
    labSection = models.CharField(max_length=50, default="000")
    meetingDays = models.CharField(max_length=10, default="")
    startingHHMM = models.CharField(max_length=10, default="0000")
    endingHHMM = models.CharField(max_length=10, default="0000")
    taEpanther = models.CharField(max_length=15, default="")
    course = models.ForeignKey(Course, on_delete=models.CASCADE, default=DEFAULT_PK)

# End of class Lab.

class Professor(models.Model):

    epanther = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    firstName = models.CharField(max_length=50, default="")
    lastName = models.CharField(max_length=50, default="")
    phoneNumber = models.CharField(max_length=50, default="")

# End of class Professor.



class TA(models.Model):

    epanther = models.CharField(max_length=50, default="")
    password = models.CharField(max_length=50, default="")
    firstName = models.CharField(max_length=50, default="")
    lastName = models.CharField(max_length=50, default="")
    phoneNumber = models.CharField(max_length=50, default="")

# End of class TA.
