from django.contrib import admin
from .models import Admin, Professor, TA, Course, Lab
# Register your models here.

admin.site.register(Admin)
admin.site.register(Course)
admin.site.register(Professor)
admin.site.register(TA)
admin.site.register(Lab)