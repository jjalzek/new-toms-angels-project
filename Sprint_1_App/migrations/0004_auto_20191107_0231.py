# Generated by Django 2.2.7 on 2019-11-07 02:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sprint_1_App', '0003_auto_20191107_0146'),
    ]

    operations = [
        migrations.AddField(
            model_name='admin',
            name='epanther',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='admin',
            name='fullName',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='admin',
            name='password',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='admin',
            name='phoneNumber',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='course',
            name='taEpantherList',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='lab',
            name='courseCode',
            field=models.CharField(default='CS000', max_length=50),
        ),
        migrations.AddField(
            model_name='lab',
            name='courseSection',
            field=models.CharField(default='000', max_length=50),
        ),
        migrations.AddField(
            model_name='lab',
            name='endingHHMM',
            field=models.CharField(default='0000', max_length=10),
        ),
        migrations.AddField(
            model_name='lab',
            name='labSection',
            field=models.CharField(default='000', max_length=50),
        ),
        migrations.AddField(
            model_name='lab',
            name='meetingDays',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AddField(
            model_name='lab',
            name='startingHHMM',
            field=models.CharField(default='0000', max_length=10),
        ),
        migrations.AddField(
            model_name='professor',
            name='courseAssignments',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='professor',
            name='firstName',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='professor',
            name='lastName',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='professor',
            name='password',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='professor',
            name='phoneNumber',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='ta',
            name='epanther',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='ta',
            name='firstName',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='ta',
            name='lastName',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='ta',
            name='password',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='ta',
            name='phoneNumber',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='courseCode',
            field=models.CharField(default='CS000', max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='courseDescription',
            field=models.CharField(default='', max_length=500),
        ),
        migrations.AlterField(
            model_name='course',
            name='courseName',
            field=models.CharField(default='Default Course', max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='courseSection',
            field=models.CharField(default='000', max_length=50),
        ),
        migrations.AlterField(
            model_name='course',
            name='endingHHMM',
            field=models.CharField(default='0000', max_length=10),
        ),
        migrations.AlterField(
            model_name='course',
            name='meetingDays',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AlterField(
            model_name='course',
            name='startingHHMM',
            field=models.CharField(default='0000', max_length=10),
        ),
        migrations.AlterField(
            model_name='professor',
            name='epanther',
            field=models.CharField(default='', max_length=50),
        ),
    ]
