# Generated by Django 2.2.7 on 2019-11-07 01:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Sprint_1_App', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='taEpantherList',
        ),
        migrations.AddField(
            model_name='professor',
            name='courseName2',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AddField(
            model_name='professor',
            name='courseSection1',
            field=models.CharField(default='', max_length=100),
        ),
    ]
