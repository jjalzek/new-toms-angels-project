"""TA_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
# Split up imported views below into separate lines so it's not just one long line
from Sprint_1_App.Views.LoginAndLogout import LoginUserToHomePage, LogoutUser

from Sprint_1_App.Views.AdminHome import AdminHome
from Sprint_1_App.Views.ProfessorHome import ProfessorHome
from Sprint_1_App.Views.TaHome import TaHome

from Sprint_1_App.Views.CreateProfessor import CreateProfessor
from Sprint_1_App.Views.CreateTA import CreateTA
from Sprint_1_App.Views.CreateCourse import CreateCourse
from Sprint_1_App.Views.CreateLab import CreateLab

from Sprint_1_App.Views.EditCourse import EditCourse
from Sprint_1_App.Views.EditLab import EditLab
from Sprint_1_App.Views.EditOwnUserInformation import EditOwnUserInfo
from Sprint_1_App.Views.EditUser import EditUser

from Sprint_1_App.Views.AssignUser import AssignProfessor, AssignTA, AssignTAToLab

urlpatterns = [
    #homepages and login urls
    path('admin/', admin.site.urls),
    path('', LoginUserToHomePage.as_view(), name='login_page'),
    path('logout', LogoutUser.as_view(), name='logout_page'),
    path('adminHome', AdminHome.as_view(), name='admin_home_page'),
    path('professorHome', ProfessorHome.as_view(), name='professor_home_page'),
    path('taHome', TaHome.as_view(), name='ta_home_page'),
    #'create' urls
    path('createProf', CreateProfessor.as_view(), name='create_professor_page'),
    path('createTA', CreateTA.as_view(), name='create_ta_page'),
    path('createCourse', CreateCourse.as_view(), name='create_course_page'),
    path('createLab', CreateLab.as_view(), name='create_lab_page'),
    #'edit' urls
    path('editCourse', EditCourse.as_view(), name='edit_course_page'),
    path('editLab', EditLab.as_view(), name='edit_lab_page'),
    path('editOwnUserInfo', EditOwnUserInfo.as_view(), name='edit_own_user_info_page'),
    path('editUser', EditUser.as_view(), name='edit_user_page'),
    #'assign' urls
    path('assignProf', AssignProfessor.as_view(), name='assign_professor_page'),
    path('assignTA', AssignTA.as_view(), name='assign_TA_page'),
    path('assignTAToLab', AssignTAToLab.as_view(), name='assign_TA_to_lab_page'),
]
