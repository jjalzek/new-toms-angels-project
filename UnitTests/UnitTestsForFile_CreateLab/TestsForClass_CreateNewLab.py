from django.test import TestCase
from Sprint_1_App.Views.CreateLab import CreateNewLab
from django.http.request import HttpRequest
from Sprint_1_App.models import Course, Lab
from UserCommandHandler.ProgramOutput import ProgramOutput

class TestFunction_attemptToCreateLab(TestCase):

    # Creating lab with course object/lab section all not in database
    def testCreateLabWithUnusedCodeAndSections(self):
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab.attemptToCreateLab()

        searchForCreatedLab = TestHelperFunctions.searchForNewlyCreatedLabObject()
        self.assertTrue(searchForCreatedLab.count() == 0)
        self.assertEquals("Error, no course exists with the entered code and section!",
                          ProgramOutput.returnProgramOutput())

    # Creating lab with course object already in database, lab section not in database
    def testCreateLabWhenCourseCodeAndSectionExists(self):
        Course.objects.create(courseCode="TST300", courseSection="400")
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab.attemptToCreateLab()

        searchForCreatedLab = TestHelperFunctions.searchForNewlyCreatedLabObject()
        self.assertTrue(searchForCreatedLab.count() == 1)
        self.assertEquals("Successfully created a new lab: TST300 400 800 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())

    # Creating lab with when lab object exists with same course code/section/lab section
    def testCreateLabWhenSameLabObjectExists(self):
        Course.objects.create(courseCode="TST300", courseSection="400")
        Lab.objects.create(courseCode="TST300", courseSection="400", labSection="800")
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab.attemptToCreateLab()

        searchForCreatedLab = TestHelperFunctions.searchForNewlyCreatedLabObject()
        self.assertTrue(searchForCreatedLab.count() == 0)
        self.assertEquals("Error, lab already exists!",
                          ProgramOutput.returnProgramOutput())

    # Creating lab with course object where course code exists, but wrong/no section number
    def testCreateLabWhenOnlyCourseCodeExists(self):
        Course.objects.create(courseCode="TST300")
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab.attemptToCreateLab()

        searchForCreatedLab = TestHelperFunctions.searchForNewlyCreatedLabObject()
        self.assertTrue(searchForCreatedLab.count() == 0)
        self.assertEquals("Error, no course exists with the entered code and section!",
                          ProgramOutput.returnProgramOutput())

    # Creating lab with course object where course section exists, but wrong/no course code
    def testCreateLabWhenOnlyCourseSectionExists(self):
        Course.objects.create(courseSection="400")
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab.attemptToCreateLab()

        searchForCreatedLab = TestHelperFunctions.searchForNewlyCreatedLabObject()
        self.assertTrue(searchForCreatedLab.count() == 0)
        self.assertEquals("Error, no course exists with the entered code and section!",
                          ProgramOutput.returnProgramOutput())

# End of class TestFunction_attemptToCreateLab

class TestFunction_obtainEnteredLabInfo(TestCase):

    def testExtractedLabInformationMatchesPassedInRequestLabInformation(self):
        CreateNewLab._request = TestHelperFunctions.createNewPostRequestContainingLabInformation()

        CreateNewLab._obtainEnteredLabInfo()

        self.assertEquals("TST300", CreateNewLab._enteredCourseCode)
        self.assertEquals("400", CreateNewLab._enteredCourseSectionNumber)
        self.assertEquals("800", CreateNewLab._enteredLabSectionNumber)
        self.assertEquals("MWF", CreateNewLab._enteredLabMeetingDays)
        self.assertEquals("1000", CreateNewLab._enteredLabStartingHHMM)
        self.assertEquals("1100", CreateNewLab._enteredLabEndingHHMM)


# End of class TestFunction__obtainEnteredLabInfo.

class TestFunction_outputNewLabInformation(TestCase):

    def testProgramOutputMatchesInformationUsedToCreateLab(self):
        TestHelperFunctions.initializeClassFieldsWithLabInformation()

        CreateNewLab._outputNewLabInformation()

        self.assertEquals("Successfully created a new lab: TST300 400 800 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())


# End of class TestFunction_outputNewLabInformation.

class TestHelperFunctions:

    @staticmethod
    def searchForNewlyCreatedLabObject():
        return Lab.objects.filter(courseCode="TST300", courseSection="400", labSection="800",
                                  meetingDays="MWF", startingHHMM="1000", endingHHMM="1100")

    @staticmethod
    def createNewPostRequestContainingLabInformation():
        request = HttpRequest()
        request.POST["courseCode"] = "TST300"
        request.POST["courseSection"] = "400"
        request.POST["labSection"] = "800"
        request.POST["days"] = "MWF"
        request.POST["startTime"] = "1000"
        request.POST["endTime"] = "1100"
        return request

    @staticmethod
    def initializeClassFieldsWithLabInformation():
        CreateNewLab._enteredCourseCode = "TST300"
        CreateNewLab._enteredCourseSectionNumber = "400"
        CreateNewLab._enteredLabSectionNumber = "800"
        CreateNewLab._enteredLabMeetingDays = "MWF"
        CreateNewLab._enteredLabStartingHHMM = "1000"
        CreateNewLab._enteredLabEndingHHMM = "1100"

# End of class TestHelperFunctions.