from django.test import TestCase
from Sprint_1_App.Views.EditUser import EditUserAccountInformation
from django.http.request import HttpRequest
from Sprint_1_App.models import Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput


class TestFunction_attemptToEditUserInformation(TestCase):

    def testUpdatedProfessorAccountMatchesPassedInRequestEditedInformation(self):
        TestHelperFunctions.createNewProfessorAccount()
        EditUserAccountInformation._request = TestHelperFunctions.createNewPostRequestContainingUpdatedUserInformation()

        EditUserAccountInformation.attemptToEditUserInformation()

        editedProfessorAccount = TestHelperFunctions.searchForRecentlyEditedProfessorAccount().first()
        self.assertEquals("USER_EPANTHER", editedProfessorAccount.epanther)
        self.assertEquals("NEW_USER_PASSWORD", editedProfessorAccount.password)
        self.assertEquals("NEW_USER_FIRST_NAME", editedProfessorAccount.firstName)
        self.assertEquals("NEW_USER_LAST_NAME", editedProfessorAccount.lastName)
        self.assertEquals("NEW_USER_PHONE_NUMBER", editedProfessorAccount.phoneNumber)


    def testUpdatedTaAccountMatchesPassedInRequestEditedInformation(self):
        TestHelperFunctions.createNewTaAccount()
        EditUserAccountInformation._request = TestHelperFunctions.createNewPostRequestContainingUpdatedUserInformation()

        EditUserAccountInformation.attemptToEditUserInformation()

        editedTaAccount = TestHelperFunctions.searchForRecentlyEditedTaAccount().first()
        self.assertEquals("USER_EPANTHER", editedTaAccount.epanther)
        self.assertEquals("NEW_USER_PASSWORD", editedTaAccount.password)
        self.assertEquals("NEW_USER_FIRST_NAME", editedTaAccount.firstName)
        self.assertEquals("NEW_USER_LAST_NAME", editedTaAccount.lastName)
        self.assertEquals("NEW_USER_PHONE_NUMBER", editedTaAccount.phoneNumber)


    # def testEditProfessorWithAlreadyUsedAdminEpanther(self):
    # def testEditProfessorWithAlreadyUsedProfessorEpanther(self):
    # def testEditProfessorWithAlreadyUsedTaEpanther(self):
    # def testEditTaWithAlreadyUsedAdminEpanther(self):
    # def testEditTaWithAlreadyUsedProfessorEpanther(self):
    # def testEditTaWithAlreadyUsedTaEpanther(self):


# End of class TestFunction_attemptToEditUserInformation.


class TestFunction_obtainEnteredUserInformation(TestCase):

    def testExtractedUpdatedInformationMatchesPassedInRequestUpdatedInformation(self):
        EditUserAccountInformation._request = TestHelperFunctions.createNewPostRequestContainingUpdatedUserInformation()

        EditUserAccountInformation._obtainEnteredUserInformation()

        self.assertEquals("USER_EPANTHER", EditUserAccountInformation._enteredEpanther)
        self.assertEquals("NEW_USER_PASSWORD", EditUserAccountInformation._enteredPassword)
        self.assertEquals("NEW_USER_FIRST_NAME", EditUserAccountInformation._enteredFirstName)
        self.assertEquals("NEW_USER_LAST_NAME", EditUserAccountInformation._enteredLastName)
        self.assertEquals("NEW_USER_PHONE_NUMBER", EditUserAccountInformation._enteredPhone)


# End of class TestFunction_obtainEnteredUserInformation.


class TestFunction_updateUserAccountInformation(TestCase):

    def testUpdatedProfessorAccountMatchesGatheredEditedInformation(self):
        TestHelperFunctions.createNewProfessorAccount()
        TestHelperFunctions.initializeClassFieldsWithUpdatedUserInformation()

        EditUserAccountInformation._updateUserAccountInformation()

        editedProfessorAccount = TestHelperFunctions.searchForRecentlyEditedProfessorAccount().first()
        self.assertEquals("USER_EPANTHER", editedProfessorAccount.epanther)
        self.assertEquals("NEW_USER_PASSWORD", editedProfessorAccount.password)
        self.assertEquals("NEW_USER_FIRST_NAME", editedProfessorAccount.firstName)
        self.assertEquals("NEW_USER_LAST_NAME", editedProfessorAccount.lastName)
        self.assertEquals("NEW_USER_PHONE_NUMBER", editedProfessorAccount.phoneNumber)


    def testUpdatedTaAccountMatchesGatheredEditedInformation(self):
        TestHelperFunctions.createNewTaAccount()
        TestHelperFunctions.initializeClassFieldsWithUpdatedUserInformation()

        EditUserAccountInformation._updateUserAccountInformation()

        editedTaAccount = TestHelperFunctions.searchForRecentlyEditedTaAccount().first()
        self.assertEquals("USER_EPANTHER", editedTaAccount.epanther)
        self.assertEquals("NEW_USER_PASSWORD", editedTaAccount.password)
        self.assertEquals("NEW_USER_FIRST_NAME", editedTaAccount.firstName)
        self.assertEquals("NEW_USER_LAST_NAME", editedTaAccount.lastName)
        self.assertEquals("NEW_USER_PHONE_NUMBER", editedTaAccount.phoneNumber)


# End of class TestFunction_updateUserAccountInformation.


class TestFunction_outputUpdatedUserAccountInformation(TestCase):

    def testProgramOutputMatchesInformationUsedToUpdateProfessor(self):
        TestHelperFunctions.initializeClassFieldsWithUpdatedUserInformation()

        EditUserAccountInformation._outputUpdatedUserAccountInformation()

        self.assertEquals("Successfully updated user information: USER_EPANTHER NEW_USER_PASSWORD "
                          "NEW_USER_FIRST_NAME NEW_USER_LAST_NAME NEW_USER_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


    def testProgramOutputMatchesInformationUsedToUpdateTa(self):
        TestHelperFunctions.initializeClassFieldsWithUpdatedUserInformation()

        EditUserAccountInformation._outputUpdatedUserAccountInformation()

        self.assertEquals("Successfully updated user information: USER_EPANTHER NEW_USER_PASSWORD "
                          "NEW_USER_FIRST_NAME NEW_USER_LAST_NAME NEW_USER_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


# End of class TestFunction_outputUpdatedUserAccountInformation.


class TestHelperFunctions:

    @staticmethod
    def createNewProfessorAccount():
        return Professor.objects.create(epanther = "USER_EPANTHER",
                                        password = "OLD_PROFESSOR_PASSWORD",
                                        firstName = "OLD_PROFESSOR_FIRST_NAME",
                                        lastName = "OLD_PROFESSOR_LAST_NAME",
                                        phoneNumber = "OLD_PROFESSOR_PHONE_NUMBER")


    @staticmethod
    def createNewTaAccount():
        return TA.objects.create(epanther = "USER_EPANTHER",
                                 password = "OLD_TA_PASSWORD",
                                 firstName = "OLD_TA_FIRST_NAME",
                                 lastName = "OLD_TA_LAST_NAME",
                                 phoneNumber = "OLD_TA_PHONE_NUMBER")


    @staticmethod
    def searchForRecentlyEditedProfessorAccount():
        return Professor.objects.filter(epanther = "USER_EPANTHER",
                                        password = "NEW_USER_PASSWORD",
                                        firstName = "NEW_USER_FIRST_NAME",
                                        lastName = "NEW_USER_LAST_NAME",
                                        phoneNumber = "NEW_USER_PHONE_NUMBER")


    @staticmethod
    def searchForRecentlyEditedTaAccount():
        return TA.objects.filter(epanther = "USER_EPANTHER",
                                 password = "NEW_USER_PASSWORD",
                                 firstName = "NEW_USER_FIRST_NAME",
                                 lastName = "NEW_USER_LAST_NAME",
                                 phoneNumber = "NEW_USER_PHONE_NUMBER")


    @staticmethod
    def createNewPostRequestContainingUpdatedUserInformation():
        request = HttpRequest()
        request.POST["epanther"] = "USER_EPANTHER"
        request.POST["tempPassword"] = "NEW_USER_PASSWORD"
        request.POST["firstName"] = "NEW_USER_FIRST_NAME"
        request.POST["lastName"] = "NEW_USER_LAST_NAME"
        request.POST["phone"] = "NEW_USER_PHONE_NUMBER"
        return request


    @staticmethod
    def initializeClassFieldsWithUpdatedUserInformation():
        EditUserAccountInformation._enteredEpanther = "USER_EPANTHER"
        EditUserAccountInformation._enteredPassword = "NEW_USER_PASSWORD"
        EditUserAccountInformation._enteredFirstName = "NEW_USER_FIRST_NAME"
        EditUserAccountInformation._enteredLastName = "NEW_USER_LAST_NAME"
        EditUserAccountInformation._enteredPhone = "NEW_USER_PHONE_NUMBER"


# End of class UsefulTestFunctions.
