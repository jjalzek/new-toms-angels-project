from django.test import TestCase
from Sprint_1_App.Views.CreateCourse import CreateNewCourse
from django.http.request import HttpRequest
from Sprint_1_App.models import Course
from UserCommandHandler.ProgramOutput import ProgramOutput


class TestFunction_attemptToCreateCourse(TestCase):

    # Creating course with course code/session not in database
    def testCreateCourseWithUnusedCodeAndSession(self):
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse.attemptToCreateCourse()

        searchForCreatedCourse = TestHelperFunctions.searchForNewlyCreatedCourseObject()
        self.assertTrue(searchForCreatedCourse.count() == 1)
        self.assertEquals("Successfully created a new course: TST300 400 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())

    # Creating course with course code/session both already in database
    def testCreateCourseWithUsedCodeAndSession(self):
        Course.objects.create(courseCode="TST300", courseSection="400")
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse.attemptToCreateCourse()

        searchForCreatedCourse = TestHelperFunctions.searchForNewlyCreatedCourseObject()
        self.assertTrue(searchForCreatedCourse.count() == 0)
        self.assertEquals("Course with this code and section already exists!",
                          ProgramOutput.returnProgramOutput())

    # Creating course with course code already in database, session number not in database
    def testCreateCourseWithUsedCodeAndUnusedSession(self):
        Course.objects.create(courseCode="TST300")
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse.attemptToCreateCourse()

        searchForCreatedCourse = TestHelperFunctions.searchForNewlyCreatedCourseObject()
        self.assertTrue(searchForCreatedCourse.count() == 1)
        self.assertEquals("Successfully created a new course: TST300 400 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())

    # Creating course with course code not in database, session number already in database
    def testCreateCourseWithUnusedCodeAndUsedSession(self):
        Course.objects.create(courseSection="400")
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse.attemptToCreateCourse()

        searchForCreatedCourse = TestHelperFunctions.searchForNewlyCreatedCourseObject()
        self.assertTrue(searchForCreatedCourse.count() == 1)
        self.assertEquals("Successfully created a new course: TST300 400 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())

    # Creating course with course code/session both in database, but in different course objects
    def testCreateCourseWithUsedCodeAndSessionDifferentObjects(self):
        Course.objects.create(courseCode="TST300")
        Course.objects.create(courseSection="400")
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse.attemptToCreateCourse()

        searchForCreatedCourse = TestHelperFunctions.searchForNewlyCreatedCourseObject()
        self.assertTrue(searchForCreatedCourse.count() == 1)
        self.assertEquals("Successfully created a new course: TST300 400 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())

# End of class TestFunction_attemptToCreateCourse

class TestFunction_obtainEnteredCourseInfo(TestCase):

    def testExtractedCourseInformationMatchesPassedInRequestCourseInformation(self):
        CreateNewCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()

        CreateNewCourse._obtainEnteredCourseInfo()

        self.assertEquals("TST300", CreateNewCourse._enteredCourseCode)
        self.assertEquals("400", CreateNewCourse._enteredCourseSectionNumber)
        self.assertEquals("MWF", CreateNewCourse._enteredCourseMeetingDays)
        self.assertEquals("1000", CreateNewCourse._enteredCourseStartingHHMM)
        self.assertEquals("1100", CreateNewCourse._enteredCourseEndingHHMM)


# End of class TestFunction__obtainEnteredCourseInfo.

class TestFunction_outputNewCourseInformation(TestCase):

    def testProgramOutputMatchesInformationUsedToCreateCourse(self):
        TestHelperFunctions.initializeClassFieldsWithCourseInformation()

        CreateNewCourse._outputNewCourseInformation()

        self.assertEquals("Successfully created a new course: TST300 400 MWF 1000 1100",
                          ProgramOutput.returnProgramOutput())


# End of class TestFunction_outputNewCourseInformation.

class TestHelperFunctions:

    @staticmethod
    def searchForNewlyCreatedCourseObject():
        return Course.objects.filter(courseCode="TST300", courseSection="400", meetingDays="MWF",
                                     startingHHMM="1000", endingHHMM="1100")

    @staticmethod
    def createNewPostRequestContainingCourseInformation():
        request = HttpRequest()
        request.POST["courseCode"] = "TST300"
        request.POST["section"] = "400"
        request.POST["days"] = "MWF"
        request.POST["startTime"] = "1000"
        request.POST["endTime"] = "1100"
        return request

    @staticmethod
    def initializeClassFieldsWithCourseInformation():
        CreateNewCourse._enteredCourseCode = "TST300"
        CreateNewCourse._enteredCourseSectionNumber = "400"
        CreateNewCourse._enteredCourseMeetingDays = "MWF"
        CreateNewCourse._enteredCourseStartingHHMM = "1000"
        CreateNewCourse._enteredCourseEndingHHMM = "1100"

# End of class TestHelperFunctions.
