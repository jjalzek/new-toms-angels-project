from django.test import TestCase
from Sprint_1_App.Views.CreateProfessor import CreateProfessorAccount
from django.http.request import HttpRequest
from Sprint_1_App.models import Admin, Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput


class TestFunction_attemptToCreateProfessor(TestCase):

    def testCreateProfessorWithUnusedEpanther(self):
        CreateProfessorAccount._request = TestHelperFunctions.createNewPostRequestContainingProfessorInformation()

        CreateProfessorAccount.attemptToCreateProfessor()

        searchForCreatedProfessorResult = TestHelperFunctions.searchForNewlyCreatedProfessorObject()
        self.assertTrue(searchForCreatedProfessorResult.count() == 1)
        self.assertEquals("Successfully created a new professor: PROFESSOR_EPANTHER PROFESSOR_PASSWORD "
                          "PROFESSOR_FIRST_NAME PROFESSOR_LAST_NAME PROFESSOR_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


    def testCreateProfessorWithAlreadyUsedAdminEpanther(self):
        Admin.objects.create(epanther = "PROFESSOR_EPANTHER")
        CreateProfessorAccount._request = TestHelperFunctions.createNewPostRequestContainingProfessorInformation()

        CreateProfessorAccount.attemptToCreateProfessor()

        searchForCreatedProfessorResult = TestHelperFunctions.searchForNewlyCreatedProfessorObject()
        self.assertTrue(searchForCreatedProfessorResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


    def testCreateProfessorWithAlreadyUsedProfessorEpanther(self):
        Professor.objects.create(epanther = "PROFESSOR_EPANTHER")
        CreateProfessorAccount._request = TestHelperFunctions.createNewPostRequestContainingProfessorInformation()

        CreateProfessorAccount.attemptToCreateProfessor()

        searchForCreatedProfessorResult = TestHelperFunctions.searchForNewlyCreatedProfessorObject()
        self.assertTrue(searchForCreatedProfessorResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


    def testCreateProfessorWithAlreadyUsedTaEpanther(self):
        TA.objects.create(epanther = "PROFESSOR_EPANTHER")
        CreateProfessorAccount._request = TestHelperFunctions.createNewPostRequestContainingProfessorInformation()

        CreateProfessorAccount.attemptToCreateProfessor()

        searchForCreatedProfessorResult = TestHelperFunctions.searchForNewlyCreatedProfessorObject()
        self.assertTrue(searchForCreatedProfessorResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


# End of class TestFunction_attemptToCreateProfessor.


class TestFunction_obtainEnteredProfessorInfo(TestCase):

    def testExtractedProfessorInformationMatchesPassedInRequestProfessorInformation(self):
        CreateProfessorAccount._request = TestHelperFunctions.createNewPostRequestContainingProfessorInformation()

        CreateProfessorAccount._obtainEnteredProfessorInfo()

        self.assertEquals("PROFESSOR_EPANTHER", CreateProfessorAccount._enteredEpanther)
        self.assertEquals("PROFESSOR_PASSWORD", CreateProfessorAccount._enteredPassword)
        self.assertEquals("PROFESSOR_FIRST_NAME", CreateProfessorAccount._enteredFirstName)
        self.assertEquals("PROFESSOR_LAST_NAME", CreateProfessorAccount._enteredLastName)
        self.assertEquals("PROFESSOR_PHONE_NUMBER", CreateProfessorAccount._enteredPhone)


# End of class TestFunction__obtainEnteredProfessorInfo.


class TestFunction_createNewProfessorAccount(TestCase):

    def testCreatedProfessorObjectMatchesGatheredProfessorInformation(self):
        TestHelperFunctions.initializeClassFieldsWithProfessorInformation()

        CreateProfessorAccount._createNewProfessorAccount()

        newlyCreatedProfessorObject = TestHelperFunctions.searchForNewlyCreatedProfessorObject().first()
        self.assertEquals("PROFESSOR_EPANTHER", newlyCreatedProfessorObject.epanther)
        self.assertEquals("PROFESSOR_PASSWORD", newlyCreatedProfessorObject.password)
        self.assertEquals("PROFESSOR_FIRST_NAME", newlyCreatedProfessorObject.firstName)
        self.assertEquals("PROFESSOR_LAST_NAME", newlyCreatedProfessorObject.lastName)
        self.assertEquals("PROFESSOR_PHONE_NUMBER", newlyCreatedProfessorObject.phoneNumber)


# End of class TestFunction__createNewProfessorAccount.


class TestFunction_outputNewProfessorInformation(TestCase):

    def testProgramOutputMatchesInformationUsedToCreateProfessor(self):
        TestHelperFunctions.initializeClassFieldsWithProfessorInformation()

        CreateProfessorAccount._outputNewProfessorInformation()

        self.assertEquals("Successfully created a new professor: PROFESSOR_EPANTHER PROFESSOR_PASSWORD "
                          "PROFESSOR_FIRST_NAME PROFESSOR_LAST_NAME PROFESSOR_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


# End of class TestFunction_outputNewProfessorInformation.


class TestHelperFunctions:

    @staticmethod
    def searchForNewlyCreatedProfessorObject():
        return Professor.objects.filter(epanther = "PROFESSOR_EPANTHER",
                                        password = "PROFESSOR_PASSWORD",
                                        firstName = "PROFESSOR_FIRST_NAME",
                                        lastName = "PROFESSOR_LAST_NAME",
                                        phoneNumber = "PROFESSOR_PHONE_NUMBER")


    @staticmethod
    def createNewPostRequestContainingProfessorInformation():
        request = HttpRequest()
        request.POST["ePanther"] = "PROFESSOR_EPANTHER"
        request.POST["tempPassword"] = "PROFESSOR_PASSWORD"
        request.POST["firstName"] = "PROFESSOR_FIRST_NAME"
        request.POST["lastName"] = "PROFESSOR_LAST_NAME"
        request.POST["phone"] = "PROFESSOR_PHONE_NUMBER"
        return request


    @staticmethod
    def initializeClassFieldsWithProfessorInformation():
        CreateProfessorAccount._enteredEpanther = "PROFESSOR_EPANTHER"
        CreateProfessorAccount._enteredPassword = "PROFESSOR_PASSWORD"
        CreateProfessorAccount._enteredFirstName = "PROFESSOR_FIRST_NAME"
        CreateProfessorAccount._enteredLastName = "PROFESSOR_LAST_NAME"
        CreateProfessorAccount._enteredPhone = "PROFESSOR_PHONE_NUMBER"


# End of class UsefulTestFunctions.
