from django.test import TestCase
from Sprint_1_App.Views.LoginAndLogout import LoginUserToHomePage


class TestFunction_redirectUserToAppropriateHomePage(TestCase):

    def testRedirectToAdminHomePageWhenAccountRoleIsAdmin(self):
        LoginUserToHomePage._userAccountRole = "admin"
        redirect = LoginUserToHomePage._redirectUserToAppropriateHomePage()
        self.assertRedirects(redirect, '/adminHome', fetch_redirect_response=False)


# End of class TestFunction_redirectUserToAppropriateHomePage.
