from django.test import TestCase
from Sprint_1_App.Views.LoginAndLogout import LoginUser
from django.http.request import HttpRequest
from Sprint_1_App.models import Admin, Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput


# class TestFunction_saveUserEpantherAssociatedWithThisSession(TestCase):
#
#     def testSessionContainsObtainedEpantherFromLogin(self):
#         LoginUser._request = self.client.request()
#         LoginUser._enteredUsername = "CORRECT_EPANTHER"
#
#         LoginUser.saveUserEpantherAssociatedWithThisSession()
#
#         self.assertEquals("CORRECT_EPANTHER", LoginUser._request.session.get('epanther'))
#
#
# # End of class TestFunction_areEnteredUsernameAndPasswordValid.


class TestFunction_areEnteredUsernameAndPasswordValid(TestCase):

    def setUp(self):
        LoginUser._request = HttpRequest()
        Professor.objects.create(epanther = "VALID_EPANTHER", password = "CORRECT_PASSWORD")


    def testInvalidUsername(self):
        LoginUser._request.POST["username"] = "I_DON'T_EXIST"
        LoginUser._request.POST["password"] = "IRRELEVANT_PASSWORD"

        self.assertFalse( LoginUser.areEnteredUsernameAndPasswordValid() )


    def testValidUsernameAndIncorrectPassword(self):
        LoginUser._request.POST["username"] = "VALID_EPANTHER"
        LoginUser._request.POST["password"] = "INCORRECT_PASSWORD"

        self.assertFalse( LoginUser.areEnteredUsernameAndPasswordValid() )


    def testValidUsernameAndCorrectPassword(self):
        LoginUser._request.POST["username"] = "VALID_EPANTHER"
        LoginUser._request.POST["password"] = "CORRECT_PASSWORD"

        self.assertTrue( LoginUser.areEnteredUsernameAndPasswordValid() )


# End of class TestFunction_areEnteredUsernameAndPasswordValid.


class TestFunction_determineUserAccountRole(TestCase):

    def setUp(self):
        Admin.objects.create(epanther = "ADMIN_EPANTHER")
        Professor.objects.create(epanther = "PROFESSOR_EPANTHER")
        TA.objects.create(epanther = "TA_EPANTHER")


    def testAdminRoleIsReturnedForAdminAccount(self):
        LoginUser._enteredEpanther = "ADMIN_EPANTHER"

        self.assertEquals("admin", LoginUser.determineUserAccountRole())


    def testProfessorRoleIsReturnedForProfessorAccount(self):
        LoginUser._enteredEpanther = "PROFESSOR_EPANTHER"

        self.assertEquals("professor", LoginUser.determineUserAccountRole())


    def testTaRoleIsReturnedForTaAccount(self):
        LoginUser._enteredEpanther = "TA_EPANTHER"

        self.assertEquals("ta", LoginUser.determineUserAccountRole())


    def test_NO_ROLE_FOUND_IsReturnedForNonexistantAccount(self):
        LoginUser._enteredEpanther = "NONEXISTANT_EPANTHER"

        self.assertEquals("NO ROLE FOUND", LoginUser.determineUserAccountRole())


# End of class TestFunction_determineUserAccountRole.


class TestFunction_obtainEnteredUsernameAndPassword(TestCase):

    def setUp(self):
        LoginUser._request = HttpRequest()
        LoginUser._request.POST["username"] = "VALID_EPANTHER"
        LoginUser._request.POST["password"] = "CORRECT_PASSWORD"


    def testEpantherAndPasswordMatchThoseInsidePassedInRequest(self):
        LoginUser._obtainEnteredUsernameAndPassword()

        self.assertEquals("VALID_EPANTHER", LoginUser._enteredEpanther)
        self.assertEquals("CORRECT_PASSWORD", LoginUser._enteredPassword)


# End of class TestFunction_obtainEnteredUsernameAndPassword.


class TestFunction_doesEpantherNotExist(TestCase):

    def setUp(self):
        Admin.objects.create(epanther = "ADMIN_EPANTHER")
        Professor.objects.create(epanther = "PROFESSOR_EPANTHER")
        TA.objects.create(epanther = "TA_EPANTHER")


    def testEpantherDoesNotExistForNonexistantAccount(self):
        LoginUser._enteredEpanther = "NONEXISTANT_EPANTHER"

        self.assertTrue( LoginUser._doesEpantherNotExist() )
        self.assertEquals("Error, username NONEXISTANT_EPANTHER does not exist.", ProgramOutput.returnProgramOutput())


    def testEpantherExistsForExistingAdminAccount(self):
        LoginUser._enteredEpanther = "ADMIN_EPANTHER"

        self.assertFalse( LoginUser._doesEpantherNotExist() )


    def testEpantherExistsForExistingProfessorAccount(self):
        LoginUser._enteredEpanther = "PROFESSOR_EPANTHER"

        self.assertFalse( LoginUser._doesEpantherNotExist() )


    def testEpantherExistsForExistingTaAccount(self):
        LoginUser._enteredEpanther = "TA_EPANTHER"

        self.assertFalse( LoginUser._doesEpantherNotExist() )


# End of class TestFunction_doesEpantherNotExist.


class TestFunction_doesEpantherNotMatchPassword(TestCase):

    def setUp(self):
        Admin.objects.create(epanther = "ADMIN_EPANTHER", password = "CORRECT_PASSWORD")
        Professor.objects.create(epanther = "PROFESSOR_EPANTHER", password = "CORRECT_PASSWORD")
        TA.objects.create(epanther = "TA_EPANTHER", password = "CORRECT_PASSWORD")


    def testAdminEpantherDoesNotMatchIncorrectPassword(self):
        LoginUser._enteredEpanther = "ADMIN_EPANTHER"
        LoginUser._enteredPassword = "INCORRECT_PASSWORD"

        self.assertTrue( LoginUser._doesEpantherNotMatchPassword() )
        self.assertEquals("Error, incorrect password entered.", ProgramOutput.returnProgramOutput())


    def testAdminEpantherDoesMatchCorrectPassword(self):
        LoginUser._enteredEpanther = "ADMIN_EPANTHER"
        LoginUser._enteredPassword = "CORRECT_PASSWORD"

        self.assertFalse( LoginUser._doesEpantherNotMatchPassword() )


    def testProfessorEpantherDoesNotMatchIncorrectPassword(self):
        LoginUser._enteredEpanther = "PROFESSOR_EPANTHER"
        LoginUser._enteredPassword = "INCORRECT_PASSWORD"

        self.assertTrue( LoginUser._doesEpantherNotMatchPassword() )
        self.assertEquals("Error, incorrect password entered.", ProgramOutput.returnProgramOutput())


    def testProfessorEpantherDoesMatchCorrectPassword(self):
        LoginUser._enteredEpanther = "PROFESSOR_EPANTHER"
        LoginUser._enteredPassword = "CORRECT_PASSWORD"

        self.assertFalse( LoginUser._doesEpantherNotMatchPassword() )


    def testTaEpantherDoesNotMatchIncorrectPassword(self):
        LoginUser._enteredEpanther = "TA_EPANTHER"
        LoginUser._enteredPassword = "INCORRECT_PASSWORD"

        self.assertTrue( LoginUser._doesEpantherNotMatchPassword() )
        self.assertEquals("Error, incorrect password entered.", ProgramOutput.returnProgramOutput())


    def testTaEpantherDoesMatchCorrectPassword(self):
        LoginUser._enteredEpanther = "TA_EPANTHER"
        LoginUser._enteredPassword = "CORRECT_PASSWORD"

        self.assertFalse( LoginUser._doesEpantherNotMatchPassword() )


# End of class TestFunction_doesEpantherNotMatchPassword.
