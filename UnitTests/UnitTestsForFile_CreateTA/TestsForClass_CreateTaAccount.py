from django.test import TestCase
from Sprint_1_App.Views.CreateTA import CreateTaAccount
from django.http.request import HttpRequest
from Sprint_1_App.models import Admin, Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput


class TestFunction_attemptToCreateTa(TestCase):

    def testCreateTaWithUnusedEpanther(self):
        CreateTaAccount._request = TestHelperFunctions.createNewPostRequestContainingTaInformation()

        CreateTaAccount.attemptToCreateTa()

        searchForCreatedTaResult = TestHelperFunctions.searchForNewlyCreatedTaObject()
        self.assertTrue(searchForCreatedTaResult.count() == 1)
        self.assertEquals("Successfully created a new TA: TA_EPANTHER TA_PASSWORD "
                          "TA_FIRST_NAME TA_LAST_NAME TA_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


    def testCreateTaWithAlreadyUsedAdminEpanther(self):
        Admin.objects.create(epanther = "TA_EPANTHER")
        CreateTaAccount._request = TestHelperFunctions.createNewPostRequestContainingTaInformation()

        CreateTaAccount.attemptToCreateTa()

        searchForCreatedTaResult = TestHelperFunctions.searchForNewlyCreatedTaObject()
        self.assertTrue(searchForCreatedTaResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


    def testCreateTaWithAlreadyUsedProfessorEpanther(self):
        Professor.objects.create(epanther = "TA_EPANTHER")
        CreateTaAccount._request = TestHelperFunctions.createNewPostRequestContainingTaInformation()

        CreateTaAccount.attemptToCreateTa()

        searchForCreatedTaResult = TestHelperFunctions.searchForNewlyCreatedTaObject()
        self.assertTrue(searchForCreatedTaResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


    def testCreateTaWithAlreadyUsedTaEpanther(self):
        TA.objects.create(epanther = "TA_EPANTHER")
        CreateTaAccount._request = TestHelperFunctions.createNewPostRequestContainingTaInformation()

        CreateTaAccount.attemptToCreateTa()

        searchForCreatedTaResult = TestHelperFunctions.searchForNewlyCreatedTaObject()
        self.assertTrue(searchForCreatedTaResult.count() == 0)
        self.assertEquals("Account with this ePanther already exists!", ProgramOutput.returnProgramOutput())


# End of class TestFunction_attemptToCreateTa.


class TestFunction_obtainEnteredTaInfo(TestCase):

    def testExtractedTaInformationMatchesPassedInRequestTaInformation(self):
        CreateTaAccount._request = TestHelperFunctions.createNewPostRequestContainingTaInformation()

        CreateTaAccount._obtainEnteredTaInfo()

        self.assertEquals("TA_EPANTHER", CreateTaAccount._enteredEpanther)
        self.assertEquals("TA_PASSWORD", CreateTaAccount._enteredPassword)
        self.assertEquals("TA_FIRST_NAME", CreateTaAccount._enteredFirstName)
        self.assertEquals("TA_LAST_NAME", CreateTaAccount._enteredLastName)
        self.assertEquals("TA_PHONE_NUMBER", CreateTaAccount._enteredPhone)


# End of class TestFunction__obtainEnteredTaInfo.


class TestFunction_createNewTaAccount(TestCase):

    def testCreatedTaObjectMatchesGatheredTaInformation(self):
        TestHelperFunctions.initializeClassFieldsWithTaInformation()

        CreateTaAccount._createNewTaAccount()

        newlyCreatedTaObject = TestHelperFunctions.searchForNewlyCreatedTaObject().first()
        self.assertEquals("TA_EPANTHER", newlyCreatedTaObject.epanther)
        self.assertEquals("TA_PASSWORD", newlyCreatedTaObject.password)
        self.assertEquals("TA_FIRST_NAME", newlyCreatedTaObject.firstName)
        self.assertEquals("TA_LAST_NAME", newlyCreatedTaObject.lastName)
        self.assertEquals("TA_PHONE_NUMBER", newlyCreatedTaObject.phoneNumber)


# End of class TestFunction__createNewTaAccount.


class TestFunction_outputNewTaInformation(TestCase):

    def testProgramOutputMatchesInformationUsedToCreateTa(self):
        TestHelperFunctions.initializeClassFieldsWithTaInformation()

        CreateTaAccount._outputNewTaInformation()

        self.assertEquals("Successfully created a new TA: TA_EPANTHER TA_PASSWORD "
                          "TA_FIRST_NAME TA_LAST_NAME TA_PHONE_NUMBER",
                          ProgramOutput.returnProgramOutput())


# End of class TestFunction_outputNewTaInformation.


class TestHelperFunctions:

    @staticmethod
    def searchForNewlyCreatedTaObject():
        return TA.objects.filter(epanther = "TA_EPANTHER",
                                 password = "TA_PASSWORD",
                                 firstName = "TA_FIRST_NAME",
                                 lastName = "TA_LAST_NAME",
                                 phoneNumber = "TA_PHONE_NUMBER")


    @staticmethod
    def createNewPostRequestContainingTaInformation():
        request = HttpRequest()
        request.POST["ePanther"] = "TA_EPANTHER"
        request.POST["tempPassword"] = "TA_PASSWORD"
        request.POST["firstName"] = "TA_FIRST_NAME"
        request.POST["lastName"] = "TA_LAST_NAME"
        request.POST["phone"] = "TA_PHONE_NUMBER"
        return request


    @staticmethod
    def initializeClassFieldsWithTaInformation():
        CreateTaAccount._enteredEpanther = "TA_EPANTHER"
        CreateTaAccount._enteredPassword = "TA_PASSWORD"
        CreateTaAccount._enteredFirstName = "TA_FIRST_NAME"
        CreateTaAccount._enteredLastName = "TA_LAST_NAME"
        CreateTaAccount._enteredPhone = "TA_PHONE_NUMBER"


# End of class UsefulTestFunctions.
