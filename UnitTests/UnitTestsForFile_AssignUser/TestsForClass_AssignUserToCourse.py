from django.test import TestCase
from Sprint_1_App.Views.AssignUser import AssignUserToCourse
from django.http.request import HttpRequest
from Sprint_1_App.models import Course, Professor, TA
from UserCommandHandler.ProgramOutput import ProgramOutput

class TestFuction_attemptToAssignProfessorToCourse(TestCase):

    #Assign professor to course with valid information
    def testAssignProfessorWithValidID(self):
        Professor.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse.attemptToAssignProfessorToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertTrue(Course.objects.filter(courseCode="COURSE100", courseSection="SECTION100", professorEpanther="EXAMPLE_EPANTHER").count() == 1)
        self.assertEquals("EXAMPLE_EPANTHER has been successfully assigned to COURSE100 - SECTION100.", ProgramOutput.returnProgramOutput())


    #Assign professor to course with invalid epanther
    def testAssignProfessorWithInvalidID(self):
        Professor.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["epanther"] = "FAKE_EPANTHER"
        AssignUserToCourse.attemptToAssignProfessorToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertEquals("Error, no professor with the username FAKE_EPANTHER was found.", ProgramOutput.returnProgramOutput())

    # Assign professor to course with invalid course
    def testAssignProfessorWithInvalidCourse(self):
        Professor.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["courseCode"] = "FAKE_COURSE"
        AssignUserToCourse.attemptToAssignProfessorToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertTrue(Course.objects.filter(courseCode="FAKE_COURSE").count() == 0)
        self.assertEquals("Error, no course was found with course code FAKE_COURSE and section number SECTION100.", ProgramOutput.returnProgramOutput())


    # Assign professor to course with invalid section number
    def testAssignProfessorWithInvalidSection(self):
        Professor.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["section"] = "FAKE_SECTION"
        AssignUserToCourse.attemptToAssignProfessorToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertTrue(Course.objects.filter(courseCode="FAKE_COURSE").count() == 0)
        self.assertEquals("Error, no course was found with course code COURSE100 and section number FAKE_SECTION.",
                          ProgramOutput.returnProgramOutput())


class TestFuction_attemptToAssignTaToCourse(TestCase):

    # Assign ta to course with valid information
    def testAssignTAWithValidID(self):
        TA.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse.attemptToAssignTaToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertEquals("EXAMPLE_EPANTHER has been successfully assigned to COURSE100 - SECTION100.",
                          ProgramOutput.returnProgramOutput())

    # Assign ta to course with invalid epanther
    def testAssignTAWithInvalidID(self):
        TA.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["epanther"] = "FAKE_EPANTHER"
        AssignUserToCourse.attemptToAssignTaToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertEquals("Error, no TA with the username FAKE_EPANTHER was found.",
                          ProgramOutput.returnProgramOutput())

    # Assign ta to course with invalid course
    def testAssignTAWithInvalidCourse(self):
        TA.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["courseCode"] = "FAKE_COURSE"
        AssignUserToCourse.attemptToAssignTaToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertTrue(Course.objects.filter(courseCode="FAKE_COURSE").count() == 0)
        self.assertEquals("Error, no course was found with course code FAKE_COURSE and section number SECTION100.",
                          ProgramOutput.returnProgramOutput())

    # Assign ta to course with invalid section number
    def testAssignTAWithInvalidSection(self):
        TA.objects.create(epanther="EXAMPLE_EPANTHER")
        Course.objects.create(courseCode="COURSE100", courseSection="SECTION100")

        AssignUserToCourse._request = TestHelperFunctions.createNewPostRequestContainingCourseInformation()
        AssignUserToCourse._request.POST["section"] = "FAKE_SECTION"
        AssignUserToCourse.attemptToAssignTaToCourse()

        newCourse = TestHelperFunctions.searchForNewlyAssignedCourseObject()

        self.assertTrue(Course.objects.filter(courseCode="FAKE_COURSE").count() == 0)
        self.assertEquals("Error, no course was found with course code COURSE100 and section number FAKE_SECTION.",
                          ProgramOutput.returnProgramOutput())



class TestHelperFunctions:

    @staticmethod
    def searchForNewlyAssignedCourseObject():
        return Course.objects.filter(courseCode="COURSE100", courseSection="SECTION100")

    @staticmethod
    def createNewPostRequestContainingCourseInformation():
        request = HttpRequest()
        request.POST["epanther"] = "EXAMPLE_EPANTHER"
        request.POST["courseCode"] = "COURSE100"
        request.POST["section"] = "SECTION100"
        return request

    @staticmethod
    def initializeClassFieldsWithCourseInformation():
        AssignUserToCourse.enteredEpantherId = "EXAMPLE_EPANTHER"
        AssignUserToCourse._enteredCourseCode = "COURSE100"
        AssignUserToCourse._enteredCourseSectionNumber = "SECTION100"
